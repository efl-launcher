/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * Copyright (C) 2009 Canonical, Ltd.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 * version 3.0 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3.0 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Authored by Gordon Allott (gord.allott@canonical.com)
 */

#ifndef _LAUNCHER_APPMAN_H_
#define _LAUNCHER_APPMAN_H_

#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS

#define LAUNCHER_TYPE_APPMAN (launcher_appman_get_type ())

#define LAUNCHER_APPMAN(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
        LAUNCHER_TYPE_APPMAN, LauncherAppman))
        
#define LAUNCHER_APPMAN_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), \
         LAUNCHER_TYPE_APPMAN, LauncherAppmanClass))
         
#define LAUNCHER_IS_APPMAN(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
        LAUNCHER_TYPE_APPMAN))
        
#define LAUNCHER_IS_APPMAN_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), \
        LAUNCHER_TYPE_APPMAN))
        
#define LAUNCHER_APPMAN_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), \
        LAUNCHER_TYPE_APPMAN, LauncherAppmanClass))

typedef struct _LauncherAppman LauncherAppman;
typedef struct _LauncherAppmanClass LauncherAppmanClass;
typedef struct _LauncherAppmanPrivate LauncherAppmanPrivate;

struct _LauncherAppmanClass
{
  GObjectClass parent_class;

};

struct _LauncherAppman
{
  GObject parent_instance;
  
  /* private */
  LauncherAppmanPrivate   *priv;
};

GType launcher_appman_get_type (void) G_GNUC_CONST;
LauncherAppman *      launcher_appman_get_default                       (void);

LauncherApplication * launcher_appman_get_application_for_desktop_file  (LauncherAppman *appman, 
                                                                         const gchar *desktop);
                                                                         
LauncherApplication * launcher_appman_get_application_for_wnck_app      (LauncherAppman *appman,
                                                                         WnckApplication *wnck_app);
                                                                         
GSequence *           launcher_appman_get_applications                  (LauncherAppman *appman);

G_END_DECLS

#endif /* _LAUNCHER_APPMAN_H_ */
