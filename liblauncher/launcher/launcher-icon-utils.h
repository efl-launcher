/*
 * Copyright (C) 2009 Canonical, Ltd.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 * version 3.0 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3.0 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Authored by Neil Jagdish Patel <neil.patel@canonical.com>
 */

#ifndef _LAUNCHER_ICON_UTILS_H_
#define _LAUNCHER_ICON_UTILS_H_

#include <glib.h>
#include <gdk/gdk.h>

G_BEGIN_DECLS


/*
 * This function is most useful for icons for volumes (disks, cd-roms, usb
 * ssh etc). If you pass a uri to a file (like file:///home/x/fun.jpg), this
 * will return you the default icon for the file type, NOT the thumbnail
 * path for that file.
 *
 * If a valid icon can't be found, this will return "folder" by default
 */
gchar * launcher_icon_utils_icon_name_for_volume_uri (const gchar *uri);

G_END_DECLS

#endif /* _LAUNCHER_ICON_UTILS_H_ */
