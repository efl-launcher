/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * Copyright (C) 2009 Canonical, Ltd.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 * version 3.0 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3.0 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Authored by Neil Jagdish Patel <neil.patel@canonical.com>
 */

#ifndef _LAUNCHER_APPLICATION_H_
#define _LAUNCHER_APPLICATION_H_

#include <glib.h>
#include <glib-object.h>
#include <libwnck/libwnck.h>

G_BEGIN_DECLS

#define LAUNCHER_TYPE_APPLICATION (launcher_application_get_type ())

#define LAUNCHER_APPLICATION(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
        LAUNCHER_TYPE_APPLICATION, LauncherApplication))
        
#define LAUNCHER_APPLICATION_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), \
        LAUNCHER_TYPE_APPLICATION, LauncherApplicationClass))
        
#define LAUNCHER_IS_APPLICATION(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
        LAUNCHER_TYPE_APPLICATION))
        
#define LAUNCHER_IS_APPLICATION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), \
        LAUNCHER_TYPE_APPLICATION))
        
#define LAUNCHER_APPLICATION_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), \
        LAUNCHER_TYPE_APPLICATION, LauncherApplicationClass))

typedef struct _LauncherApplication LauncherApplication;
typedef struct _LauncherApplicationClass LauncherApplicationClass;
typedef struct _LauncherApplicationPrivate LauncherApplicationPrivate;
typedef struct _LauncherApplicationWindow LauncherApplicationWindow;

struct _LauncherApplicationClass
{
  GObjectClass parent_class;

  /* Signals */
  void(* opened) (LauncherApplication *self, WnckApplication *wnckapp);
  void(* closed) (LauncherApplication *self, WnckApplication *wnckapp);
};


struct _LauncherApplication
{
  GObject parent_instance;

  /* Private */
  LauncherApplicationPrivate *priv;
};

GType launcher_application_get_type (void) G_GNUC_CONST;

LauncherApplication * launcher_application_new (void);
LauncherApplication * launcher_application_new_from_desktop_file (const gchar *desktop_file);
LauncherApplication * launcher_application_new_from_wnck_app (WnckApplication *app);

gboolean      launcher_application_launch             (LauncherApplication *application, 
                                                       GError **error);

const gchar * launcher_application_get_unique_string  (LauncherApplication *application);

GSList *      launcher_application_get_wnckapps       (LauncherApplication *application);
void          launcher_application_add_wnckapp        (LauncherApplication *application,
                                                       WnckApplication     *wnck_app);

const gchar * launcher_application_get_name           (LauncherApplication *application);

const gchar * launcher_application_get_comment        (LauncherApplication *application);

const gchar * launcher_application_get_icon_name      (LauncherApplication *application);

const gchar * launcher_application_get_exec_string    (LauncherApplication *application);

const gchar * launcher_application_get_desktop_file   (LauncherApplication *application);
void          launcher_application_set_desktop_file   (LauncherApplication *application,
                                                       gchar *desktop_file);

const gchar * launcher_application_get_unique_string  (LauncherApplication *application);

gboolean      launcher_application_get_running        (LauncherApplication *application);

gboolean      launcher_application_get_favorite       (LauncherApplication *application);

GSList *      launcher_application_get_categories     (LauncherApplication *application);

gboolean      launcher_application_get_focused        (LauncherApplication *application);
void          launcher_application_set_focused        (LauncherApplication *application, 
                                                       WnckWindow *window);

void          launcher_application_show               (LauncherApplication *application);

G_END_DECLS

#endif /* _LAUNCHER_APPLICATION_H_ */


