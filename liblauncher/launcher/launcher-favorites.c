/*
 * Copyright (C) 2009 Canonical, Ltd.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 * version 3.0 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3.0 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Authored by Neil Jagdish Patel <neil.patel@canonical.com>
 */
/**
 * SECTION:launcher-favorites
 * @short_description: Stores a users 'favorite' applications for easy access
 *
 * #LauncherFavorites will store favorite #LauncherApplication applications for easy access
 */

 
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "launcher-favorites.h"

#include <gconf/gconf.h>
#include <gconf/gconf-client.h>

G_DEFINE_TYPE (LauncherFavorites, launcher_favorites, G_TYPE_OBJECT)

#define LAUNCHER_FAVORITES_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE(obj, \
        LAUNCHER_TYPE_FAVORITES, LauncherFavoritesPrivate))

#define FAV_PATH "/apps/netbook-launcher/favorites"
#define FAV_LIST FAV_PATH"/favorites_list"

struct _LauncherFavoritesPrivate
{
  GConfClient *client;
};

static LauncherFavorites *launcher_favorites = NULL;

/* Forwards */

/* GObject Init */

static void
launcher_favorites_finalize (GObject *favorites)
{
  LauncherFavoritesPrivate *priv;

  g_return_if_fail (LAUNCHER_IS_FAVORITES (favorites));
  priv = LAUNCHER_FAVORITES (favorites)->priv;

  if (priv->client)
    {
      g_object_unref (priv->client);
      priv->client = NULL;
    }

  G_OBJECT_CLASS (launcher_favorites_parent_class)->finalize (favorites);
}

static void
launcher_favorites_class_init (LauncherFavoritesClass *klass)
{
  GObjectClass *obj_class = G_OBJECT_CLASS (klass);

  obj_class->finalize = launcher_favorites_finalize;

  g_type_class_add_private (obj_class, sizeof (LauncherFavoritesPrivate));
}

static void
launcher_favorites_init (LauncherFavorites *favorites)
{
  LauncherFavoritesPrivate *priv;

  priv = favorites->priv = LAUNCHER_FAVORITES_GET_PRIVATE (favorites);

  priv->client = gconf_client_get_default ();
}

LauncherFavorites *
launcher_favorites_get_default (void)
{
  if (G_UNLIKELY (!LAUNCHER_IS_FAVORITES (launcher_favorites)))
    {
      launcher_favorites = g_object_new (LAUNCHER_TYPE_FAVORITES,
                                         NULL);
      return launcher_favorites;
    }

  return g_object_ref (launcher_favorites);
}

gchar *
get_path (const gchar *uid, const gchar *key)
{
  return g_strdup_printf ("%s/%s/%s", FAV_PATH, uid, key);
}

/*
 * Public Methods
 */
/**
 * launcher_favorites_get_favorites:
 * @favorites: A #LauncherFavorites object
 *
 * retrives a #GCONF_VALUE_LIST of favorite applications 
 *
 * Returns: An unowned #GSList
 */
GSList *
launcher_favorites_get_favorites (LauncherFavorites *favorites)
{
  LauncherFavoritesPrivate *priv;

  g_return_val_if_fail (LAUNCHER_IS_FAVORITES (favorites), NULL);
  priv = favorites->priv;

  return gconf_client_get_list (priv->client,
                                FAV_LIST, GCONF_VALUE_STRING,
                                NULL);
}

/**
 * launcher_favorites_set_favorites:
 * @self: a #LauncherFavorites object
 * @list: A #GSList object
 *
 * Allows you to set all the favorite applications at once using one gslist, 
 * see gconf_client_set_list() for list stucture
 */
void
launcher_favorites_set_favorites (LauncherFavorites *self,
                                  GSList            *list)
{
  LauncherFavoritesPrivate *priv;

  g_return_if_fail (LAUNCHER_IS_FAVORITES (self));
  priv = self->priv;

  gconf_client_set_list (priv->client, FAV_LIST,
                         GCONF_VALUE_STRING, list,
                         NULL);
}

/**
 * launcher_favorites_add_favorite:
 * @favorites: a #LauncherFavorites object
 * @uid: a unique string
 *
 * Allows you to add a new favorite to the list of favorites, you need to supply
 * a unique id 
 *
 * Returns: A boolean indicating wheather the function completed succesfully or not
 */
gboolean
launcher_favorites_add_favorite (LauncherFavorites *favorites,
                                 const gchar   *uid)
{
  LauncherFavoritesPrivate *priv;
  gboolean  ret = TRUE;
  GSList   *favs, *f;
  GSList   *new_favs = NULL;
  GError   *error = NULL;

  g_return_val_if_fail (LAUNCHER_IS_FAVORITES (favorites), FALSE);
  g_return_val_if_fail (uid, FALSE);
  priv = favorites->priv;

  favs = gconf_client_get_list (priv->client,
                                FAV_LIST, GCONF_VALUE_STRING,
                                NULL);

  for (f = favs; f; f = f->next)
    new_favs = g_slist_append (new_favs, g_strdup (f->data));

  /* FIXME: Do dup checks */
  new_favs = g_slist_append (new_favs, g_strdup (uid));
  gconf_client_set_list (priv->client, FAV_LIST, GCONF_VALUE_STRING,
                         new_favs, &error);

  if (error)
    {
      g_warning ("Unable to add favorite %s: %s", uid, error->message);
      g_error_free (error);
      ret = FALSE;
    }

  g_slist_foreach (favs, (GFunc)g_free, NULL);
  g_slist_free (favs);
  g_slist_foreach (new_favs, (GFunc)g_free, NULL);
  g_slist_free (new_favs);

  return ret;
}

/**
 * launcher_favorites_remove_favorite:
 * @favorites: a #LauncherFavorites object
 * @uid: a unique string
 *
 * Allows you to remove a favorite to the list of favorites, you need to supply
 * a unique id 
 *
 * Returns: A boolean indicating wheather the function completed succesfully or not
 */
gboolean
launcher_favorites_remove_favorite (LauncherFavorites *favorites,
                                    const gchar       *uid)
{
  LauncherFavoritesPrivate *priv;
  gboolean     ret = TRUE;
  GSList      *favs, *f;
  GSList      *new_favs = NULL;
  GError      *error = NULL;
  gchar       *directory;

  g_return_val_if_fail (LAUNCHER_IS_FAVORITES (favorites), FALSE);
  g_return_val_if_fail (uid, FALSE);
  priv = favorites->priv;

  favs = gconf_client_get_list (priv->client,
                                FAV_LIST, GCONF_VALUE_STRING,
                                NULL);

  for (f = favs; f; f = f->next)
    {
      if (g_strcmp0 (f->data, uid) != 0)
        new_favs = g_slist_append (new_favs, g_strdup (f->data));
      else
        g_debug ("Removing favorite: %s", (gchar*)f->data);
    }

  gconf_client_set_list (priv->client, FAV_LIST, GCONF_VALUE_STRING,
                         new_favs, &error);

  if (error)
    {
      g_warning ("Unable to remove favorite %s: %s", uid, error->message);
      g_error_free (error);
      ret = FALSE;
    }

  directory = g_strdup_printf ("%s/%s", FAV_PATH, uid);
  gconf_client_recursive_unset (priv->client, directory, 0, &error);
  if (error)
    {
      g_warning ("Unable to remove favorite directory %s: %s", directory,
                 error->message);
      g_error_free (error);
    }

  g_slist_foreach (favs, (GFunc)g_free, NULL);
  g_slist_free (favs);
  g_slist_foreach (new_favs, (GFunc)g_free, NULL);
  g_slist_free (new_favs);
  g_free (directory);

  return ret;
}

gchar *
launcher_favorites_get_string (LauncherFavorites *favorites,
                               const gchar   *uid,
                               const gchar   *key)
{
  LauncherFavoritesPrivate *priv;
  gchar  *path;
  gchar  *ret;
  GError *error = NULL;

  g_return_val_if_fail (LAUNCHER_IS_FAVORITES (favorites), NULL);
  g_return_val_if_fail (uid, NULL);
  g_return_val_if_fail (key, NULL);
  priv = favorites->priv;

  path = get_path (uid, key);
  ret = gconf_client_get_string (priv->client, path, &error);
  g_free (path);

  if (error)
    {
      g_warning ("Unable to key string for path %s: %s", path, error->message);
      g_error_free (error);
    }

  return ret;
}

void
launcher_favorites_set_string (LauncherFavorites *favorites,
                               const gchar   *uid,
                               const gchar   *key,
                               const gchar   *value)
{
  LauncherFavoritesPrivate *priv;
  gchar  *path;
  GError *error = NULL;

  g_return_if_fail (LAUNCHER_IS_FAVORITES (favorites));
  g_return_if_fail (uid);
  g_return_if_fail (key);
  g_return_if_fail (value);
  priv = favorites->priv;

  path = get_path (uid, key);
  gconf_client_set_string (priv->client, path, value, &error);
  g_free (path);

  if (error)
    {
      g_warning ("Unable to key string for path %s: %s", path, error->message);
      g_error_free (error);
    }
}


gint
launcher_favorites_get_int    (LauncherFavorites *favorites,
                               const gchar *uid,
                               const gchar *key)
{
  LauncherFavoritesPrivate *priv;
  gint    ret;
  gchar  *path;
  GError *error = NULL;

  g_return_val_if_fail (LAUNCHER_IS_FAVORITES (favorites), 0);
  g_return_val_if_fail (uid, 0);
  g_return_val_if_fail (key, 0);
  priv = favorites->priv;

  path = get_path (uid, key);
  ret = gconf_client_get_int (priv->client, path, &error);
  g_free (path);

  if (error)
    {
      g_warning ("Unable to get int for path %s: %s", path, error->message);
      g_error_free (error);
    }

  return ret;
}


void
launcher_favorites_set_int    (LauncherFavorites *favorites,
                               const gchar *uid,
                               const gchar *key,
                               gint         value)
{
  LauncherFavoritesPrivate *priv;
  gchar *path;
  GError *error = NULL;

  g_return_if_fail (LAUNCHER_IS_FAVORITES (favorites));
  g_return_if_fail (uid);
  g_return_if_fail (key);
  priv = favorites->priv;

  path = get_path (uid, key);
  gconf_client_set_int (priv->client, path, value, &error);
  g_free (path);

  if (error)
    {
      g_warning ("Unable to set int for path %s: %s", path, error->message);
      g_error_free (error);
    }
}


gfloat
launcher_favorites_get_float  (LauncherFavorites *favorites,
                               const gchar *uid,
                               const gchar *key)
{
  LauncherFavoritesPrivate *priv;
  gfloat  ret;
  gchar  *path;
  GError *error = NULL;

  g_return_val_if_fail (LAUNCHER_IS_FAVORITES (favorites), 0);
  g_return_val_if_fail (uid, 0);
  g_return_val_if_fail (key, 0);
  priv = favorites->priv;

  path = get_path (uid, key);
  ret = gconf_client_get_float (priv->client, path, &error);
  g_free (path);

  if (error)
    {
      g_warning ("Unable to get float for path %s: %s", path, error->message);
      g_error_free (error);
    }

  return ret;
}


void
launcher_favorites_set_float  (LauncherFavorites *favorites,
                               const gchar   *uid,
                               const gchar   *key,
                               gfloat         value)
{
  LauncherFavoritesPrivate *priv;
  gchar  *path;
  GError *error = NULL;

  g_return_if_fail (LAUNCHER_IS_FAVORITES (favorites));
  g_return_if_fail (uid);
  g_return_if_fail (key);
  priv = favorites->priv;

  path = get_path (uid, key);
  gconf_client_set_float (priv->client, path, value, &error);
  g_free (path);

  if (error)
    {
      g_warning ("Unable to set float for path %s: %s", path, error->message);
      g_error_free (error);
    }
}

gboolean
launcher_favorites_get_bool   (LauncherFavorites *favorites,
                               const gchar    *uid,
                               const gchar    *key)
{
  LauncherFavoritesPrivate *priv;
  gboolean  ret;
  gchar    *path;
  GError   *error = NULL;

  g_return_val_if_fail (LAUNCHER_IS_FAVORITES (favorites), FALSE);
  g_return_val_if_fail (uid, FALSE);
  g_return_val_if_fail (key, FALSE);
  priv = favorites->priv;

  path = get_path (uid, key);
  ret = gconf_client_get_bool (priv->client, path, &error);
  g_free (path);

  if (error)
    {
      g_warning ("Unable to get bool for path %s: %s", path, error->message);
      g_error_free (error);
    }

  return ret;
}

void
launcher_favorites_set_bool   (LauncherFavorites *favorites,
                               const gchar   *uid,
                               const gchar   *key,
                               gboolean       value)
{
  LauncherFavoritesPrivate *priv;
  gchar  *path;
  GError *error = NULL;

  g_return_if_fail (LAUNCHER_IS_FAVORITES (favorites));
  g_return_if_fail (uid);
  g_return_if_fail (key);
  priv = favorites->priv;

  path = get_path (uid, key);
  gconf_client_set_bool (priv->client, path, value, &error);
  g_free (path);

  if (error)
    {
      g_warning ("Unable to set bool for path %s: %s", path, error->message);
      g_error_free (error);
    }
}

GSList *
launcher_favorites_get_list   (LauncherFavorites *favorites,
                               const gchar   *uid,
                               const gchar   *key,
                               LauncherFavoritesListValue value_type)
{
  LauncherFavoritesPrivate *priv;
  GSList         *ret;
  gchar          *path;
  GError         *error = NULL;
  GConfValueType  type;

  g_return_val_if_fail (LAUNCHER_IS_FAVORITES (favorites), NULL);
  g_return_val_if_fail (uid, NULL);
  g_return_val_if_fail (key, NULL);
  priv = favorites->priv;

  switch (value_type)
    {
    case LAUNCHER_FAVORITES_STRING:
      type = GCONF_VALUE_STRING;
      break;
    case LAUNCHER_FAVORITES_INT:
      type = GCONF_VALUE_INT;
      break;
    case LAUNCHER_FAVORITES_FLOAT:
      type = GCONF_VALUE_FLOAT;
      break;
    case LAUNCHER_FAVORITES_BOOL:
      type = GCONF_VALUE_BOOL;
      break;

    default:
      g_warning ("Unsupported list type %d", value_type);
      return NULL;
    }

  path = get_path (uid, key);
  ret = gconf_client_get_list (priv->client, path, type, &error);
  g_free (path);

  if (error)
    {
      g_warning ("Unable to get list for path %s: %s", path, error->message);
      g_error_free (error);
    }

  return ret;
}

void
launcher_favorites_set_list   (LauncherFavorites *favorites,
                               const gchar   *uid,
                               const gchar   *key,
                               LauncherFavoritesListValue value_type,
                               GSList        *value)
{
  LauncherFavoritesPrivate *priv;
  GConfValueType  type;
  gchar          *path;
  GError         *error = NULL;

  g_return_if_fail (LAUNCHER_IS_FAVORITES (favorites));
  g_return_if_fail (uid);
  g_return_if_fail (key);
  priv = favorites->priv;

  switch (value_type)
    {
    case LAUNCHER_FAVORITES_STRING:
      type = GCONF_VALUE_STRING;
      break;
    case LAUNCHER_FAVORITES_INT:
      type = GCONF_VALUE_INT;
      break;
    case LAUNCHER_FAVORITES_FLOAT:
      type = GCONF_VALUE_FLOAT;
      break;
    case LAUNCHER_FAVORITES_BOOL:
      type = GCONF_VALUE_BOOL;
      break;

    default:
      g_warning ("Unsupported list type %d", value_type);
      return;
    }

  path = get_path (uid, key);
  gconf_client_set_list (priv->client, path, type, value, &error);
  g_free (path);

  if (error)
    {
      g_warning ("Unable to get list for path %s: %s", path, error->message);
      g_error_free (error);
    }
}
