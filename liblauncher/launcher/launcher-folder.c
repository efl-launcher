/*
 * Copyright (C) 2009 Canonical, Ltd.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 * version 3.0 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3.0 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Authored by Neil Jagdish Patel <neil.patel@canonical.com>
 */
/**
 * SECTION:launcher-folder
 * @short_description: stores and launches (if needed) folder information
 *
 * Will store folder information, generally returned by #LauncherFolderBookmarks
 * Can also launch folders if needed
 */
#if HAVE_CONFIG_H
#include <config.h>
#endif

#include "launcher-folder.h"
#include "launcher-icon-utils.h"

struct _LauncherFolder
{
  gchar *name;
  gchar *uri;
  gchar *icon_name;
};

static LauncherFolder *
launcher_folder_copy (const LauncherFolder *folder)
{
  if (G_LIKELY (folder != NULL))
    {
      LauncherFolder *ret = NULL;

      ret = g_slice_new0 (LauncherFolder);
      ret->name = g_strdup (folder->name);
      ret->uri = g_strdup (folder->uri);
      ret->icon_name = folder->icon_name ? g_strdup (folder->icon_name) : NULL;
      return ret;
    }
  return NULL;
}

void
launcher_folder_free (LauncherFolder *folder)
{
  if (G_LIKELY (folder != NULL))
    {
      g_free (folder->name);
      g_free (folder->uri);
      g_free (folder->icon_name);

      g_slice_free (LauncherFolder, folder);
    }
}

GType
launcher_folder_get_type (void)
{
  static GType _launcher_folder_type = 0;

  if (G_UNLIKELY (_launcher_folder_type == 0))
    {
      _launcher_folder_type =
        g_boxed_type_register_static ("LauncherFolder",
                                      (GBoxedCopyFunc) launcher_folder_copy,
                                      (GBoxedFreeFunc) launcher_folder_free);

    }

  return _launcher_folder_type;
}

/*
 * Public Methods
 */
/**
 * launcher_folder_new:
 * @name: A string containing the folder name
 * @uri: A string containing the URI to the folder location
 *
 * Creates a new #LauncherFolder object based on the @name and @uri given 
 *
 * Returns: A new #LauncherFolder object
 */
LauncherFolder *
launcher_folder_new (const gchar  *name,
                     const gchar  *uri)
{
  LauncherFolder *folder;

  folder = g_slice_new0 (LauncherFolder);

  if (folder != NULL)
    {
      folder->name = g_strdup (name);
      folder->uri = g_strdup (uri);
      folder->icon_name = NULL;
    }

  return folder;
}

/**
 * launcher_folder_get_name:
 * @folder: A #LauncherFolder object
 *
 * Retrives the name of the @folder
 *
 * Returns: A string containing the @folder name
 */
const gchar *
launcher_folder_get_name (LauncherFolder *folder)
{
  g_return_val_if_fail (folder, NULL);
  return folder->name;
}

/**
 * launcher_folder_get_uri:
 * @folder: A #LauncherFolder object
 *
 * Retrives the uri of the @folder
 *
 * Returns: A string containing the @folder uri
 */
const gchar *
launcher_folder_get_uri (LauncherFolder *folder)
{
  g_return_val_if_fail (folder, NULL);
  return folder->uri;
}

/**
 * launcher_folder_get_icon_name:
 * @folder: A #LauncherFolder object
 *
 * retrives the icon name that is associated with @folder
 *
 * Returns: An icon_name string
 */
const gchar *
launcher_folder_get_icon_name (LauncherFolder *folder)
{
  g_return_val_if_fail (folder, NULL);

  if (folder->icon_name == NULL)
    {
      folder->icon_name = launcher_icon_utils_icon_name_for_volume_uri (folder->uri);
    }

  return folder->icon_name;
}

static void
on_places_mount (GFile *file, GAsyncResult *res, LauncherFolder *folder)
{
  GError *error = NULL;
  gboolean success;

  success = g_file_mount_enclosing_volume_finish (file, res, &error);

  if (success)
    {
      g_app_info_launch_default_for_uri (g_file_get_uri (file), NULL, &error);
    }
  else
    {
      /*    GtkWidget *dialog;
          gchar *uri;

          uri = g_file_get_uri (file);

          dialog = gtk_message_dialog_new_with_markup (NULL,
                                           0,
                                           GTK_MESSAGE_ERROR,
                                           GTK_BUTTONS_CLOSE,
                                           _("<b>Unable to open %s:</b>\n%s"),
                                           uri,
                                           error ? error->message : _("Unknown"));
          gtk_dialog_run (GTK_DIALOG (dialog));
          gtk_widget_destroy (dialog);
          g_error_free (error);
          g_free (uri);*/

      g_warning ("Unable to launch folder: %s", error->message);
      g_error_free (error);
    }

  g_object_unref (file);
}


/**
 * launcher_folder_activate:
 * @folder: A #LauncherFolder object
 * @error: A pointer to a GError
 *
 * Will launch the correct application for @folder, mounting the volume if necessary
 */
void
launcher_folder_activate (LauncherFolder *folder, GError **error)
{
  GVfs      *vfs;
  GFile     *file;
  GFileInfo *info;

  g_return_if_fail (folder);
  g_return_if_fail (*error == NULL);

  vfs = g_vfs_get_default ();
  file = g_vfs_get_file_for_uri (vfs, folder->uri);

  info = g_file_query_filesystem_info (file,
                                       G_FILE_ATTRIBUTE_MOUNTABLE_CAN_MOUNT,
                                       NULL, NULL);

  if (!info)
    {
      GMount *mount;

      mount = g_file_find_enclosing_mount (file, NULL, NULL);

      if (mount)
        {
          g_app_info_launch_default_for_uri (folder->uri, NULL, error);
        }
      else
        {
          g_file_mount_enclosing_volume (file, 0, NULL, NULL,
                                         (GAsyncReadyCallback)on_places_mount,
                                         folder);
          return;
        }
    }
  else if (g_file_info_get_attribute_boolean (info,
           G_FILE_ATTRIBUTE_MOUNTABLE_CAN_MOUNT))
    {
      GMount *mount;

      mount = g_file_find_enclosing_mount (file, NULL, NULL);

      if (mount)
        {
          g_app_info_launch_default_for_uri (folder->uri, NULL, error);
        }
      else
        {
          g_file_mount_enclosing_volume (file, 0, NULL, NULL,
                                         (GAsyncReadyCallback)on_places_mount,
                                         folder);
          return;
        }
    }
  else
    {
      g_app_info_launch_default_for_uri (folder->uri, NULL, error);
    }

  if (info)
    g_object_unref (info);
  g_object_unref (file);
}
