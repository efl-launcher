/*
 * Copyright (C) 2009 Canonical, Ltd.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 * version 3.0 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3.0 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Authored by Neil Jagdish Patel <neil.patel@canonical.com>
 */

#ifndef _HAVE_LAUNCHER_FOLDER_BOOKMARKS_H
#define _HAVE_LAUNCHER_FOLDER_BOOKMARKS_H

#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS

#define LAUNCHER_TYPE_FOLDER_BOOKMARKS (launcher_folder_bookmarks_get_type ())

#define LAUNCHER_FOLDER_BOOKMARKS(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
        LAUNCHER_TYPE_FOLDER_BOOKMARKS, LauncherFolderBookmarks))

#define LAUNCHER_FOLDER_BOOKMARKS_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), \
        LAUNCHER_TYPE_FOLDER_BOOKMARKS, LauncherFolderBookmarksClass))

#define LAUNCHER_IS_FOLDER_BOOKMARKS(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
        LAUNCHER_TYPE_FOLDER_BOOKMARKS))

#define LAUNCHER_IS_FOLDER_BOOKMARKS_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), \
        LAUNCHER_TYPE_FOLDER_BOOKMARKS))

#define LAUNCHER_FOLDER_BOOKMARKS_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), \
        LAUNCHER_TYPE_FOLDER_BOOKMARKS, LauncherFolderBookmarksClass))

typedef struct _LauncherFolderBookmarks LauncherFolderBookmarks;
typedef struct _LauncherFolderBookmarksClass LauncherFolderBookmarksClass;
typedef struct _LauncherFolderBookmarksPrivate LauncherFolderBookmarksPrivate;

struct _LauncherFolderBookmarks
{
  GObject         parent;

  /*< private >*/
  LauncherFolderBookmarksPrivate   *priv;
};

struct _LauncherFolderBookmarksClass
{
  GObjectClass    parent_class;

  /*< public >*/
  void (*bookmarks_changed) (LauncherFolderBookmarks *folder_bookmarks);

  /*< private >*/
  void (*_launcher_folder_bookmarks_1) (void);
  void (*_launcher_folder_bookmarks_2) (void);
  void (*_launcher_folder_bookmarks_3) (void);
  void (*_launcher_folder_bookmarks_4) (void);
};

GType                 launcher_folder_bookmarks_get_type    (void) G_GNUC_CONST;

/*
 * Returns the LauncherFolderBookmarks singleton. Increments the ref_count so
 * be sure to g_object_unref it once your done
 */
LauncherFolderBookmarks * launcher_folder_bookmarks_get_default (void);

/*
 * A GSList of LauncherFolder pointers.
 * This list, and it's contents, are owned by the LauncherFolderBookmarks instance, * please do not modify or free it.
 */
GSList * launcher_folder_bookmarks_get_bookmarks (LauncherFolderBookmarks *bookmarks);

G_END_DECLS

#endif /* _HAVE_LAUNCHER_FOLDER_BOOKMARKS_H */
