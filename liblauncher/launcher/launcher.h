/*
 * Copyright (C) 2009 Canonical, Ltd.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 * version 3.0 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3.0 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Authored by Neil Jagdish Patel <neil.patel@canonical.com>
 */

#ifndef _LAUNCHER_H_
#define _LAUNCHER_H_

#include <launcher/launcher-application.h>
#include <launcher/launcher-appman.h>
#include <launcher/launcher-category.h>
#include <launcher/launcher-favorites.h>
#include <launcher/launcher-folder-bookmarks.h>
#include <launcher/launcher-folder.h>
#include <launcher/launcher-icon-utils.h>
#include <launcher/launcher-menu.h>
#include <launcher/launcher-session.h>

#endif /* _LAUNCHER_H_ */
