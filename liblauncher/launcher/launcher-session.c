/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * Copyright (C) 2009 Canonical, Ltd.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 * version 3.0 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3.0 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Authored by Neil Jagdish Patel <neil.patel@canonical.com>
 */
/**
 * SECTION:launcher-session
 * @short_description: Handles acquiring data from the current session
 * @include: launcher-session.h
 *
 * #LauncherSession objects exist to query data from the current session, 
 * Examples of this type of data include the workarea size and currently running applications
 */
 

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gdk/gdkx.h>
#include <X11/Xatom.h>
#include <X11/Xutil.h>
#include <libwncksync/libwncksync.h>

#include "launcher-application.h"
#include "launcher-appman.h"
#include "launcher-menu.h"

#include "launcher-session.h"

G_DEFINE_TYPE (LauncherSession, launcher_session, G_TYPE_OBJECT)

#define LAUNCHER_SESSION_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE(obj, \
        LAUNCHER_TYPE_SESSION, LauncherSessionPrivate))

#define LAUNCHERAPP_ID "launcher-application-id"

struct _LauncherSessionPrivate
{
  /* Application variables */
  WnckScreen *screen;
  GSList *running_apps;
};

enum
{
  APP_OPENED,
  APP_LAUNCHING,

  LAST_SIGNAL
};

static guint _session_signals[LAST_SIGNAL] = { 0 };

static LauncherSession *launcher_session = NULL;

/* Forwards */
static void on_application_opened (WnckScreen      *screen,
                                   WnckApplication *app,
                                   LauncherSession     *session);

static void
on_active_window_changed (WnckScreen *screen,
                          WnckWindow *previously_active_window,
                          LauncherSession *session);

/* GObject Init */
static void
launcher_session_finalize (GObject *session)
{
  LauncherSessionPrivate *priv;

  g_return_if_fail (LAUNCHER_IS_SESSION (session));
  priv = LAUNCHER_SESSION (session)->priv;

  if (priv->running_apps)
    {
      g_slist_foreach (priv->running_apps, (GFunc)g_object_unref, NULL);
      g_slist_free (priv->running_apps);
      priv->running_apps = NULL;
    }
  /* Note: We don't ref/unref priv->screen as-per-wnck-docs */
  priv->screen = NULL;

  G_OBJECT_CLASS (launcher_session_parent_class)->finalize (session);
}

static void
launcher_session_class_init (LauncherSessionClass *klass)
{
  GObjectClass *obj_class = G_OBJECT_CLASS (klass);

  obj_class->finalize = launcher_session_finalize;

  _session_signals[APP_LAUNCHING] =
    g_signal_new ("application-launching",
                  G_OBJECT_CLASS_TYPE (obj_class),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (LauncherSessionClass, application_launching),
                  NULL, NULL,
                  g_cclosure_marshal_VOID__POINTER,
                  G_TYPE_NONE, 1, G_TYPE_POINTER);

  _session_signals[APP_OPENED] =
    g_signal_new ("application-opened",
                  G_OBJECT_CLASS_TYPE (obj_class),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (LauncherSessionClass, application_opened),
                  NULL, NULL,
                  g_cclosure_marshal_VOID__POINTER,
                  G_TYPE_NONE, 1, G_TYPE_POINTER);
  g_type_class_add_private (obj_class, sizeof (LauncherSessionPrivate));
}

static void
launcher_session_init (LauncherSession *session)
{
  LauncherSessionPrivate *priv;

  priv = session->priv = LAUNCHER_SESSION_GET_PRIVATE (session);

  wnck_set_client_type (WNCK_CLIENT_TYPE_PAGER);

  /* Grab WnckScreen and connect to the important signals */
  priv->screen = wnck_screen_get_default ();
  g_signal_connect (priv->screen, "application-opened",
                    G_CALLBACK (on_application_opened), session);

  g_signal_connect (priv->screen, "active-window-changed",
                    G_CALLBACK (on_active_window_changed), session);
}

LauncherSession *
launcher_session_get_default (void)
{
  if (G_UNLIKELY (!LAUNCHER_IS_SESSION (launcher_session)))
    {
      wncksync_init ();
      launcher_session = g_object_new (LAUNCHER_TYPE_SESSION,
                                       NULL);
      return launcher_session;
    }

  return g_object_ref (launcher_session);
}

/*
 * Private methods
 */

static void
on_application_opened (WnckScreen      *screen,
                       WnckApplication *app,
                       LauncherSession *session)
{
  LauncherSessionPrivate *priv;
  gchar              *app_name = NULL;
  gchar              *res_name = NULL;
  gchar              *class_name = NULL;
  LauncherApplication    *bestmatch = NULL;
  LauncherAppman         *appman = NULL;
  GList              *windows, *w;


  g_return_if_fail (LAUNCHER_IS_SESSION (session));
  g_return_if_fail (WNCK_IS_APPLICATION (app));
  priv = session->priv;
  
  appman = launcher_appman_get_default ();
  
  /* first we want to attempt a match to a desktop file with libwncksync */
  // grab a list of windows, loop though until we get a match
  windows = wnck_application_get_windows (app);
  for (w = windows; w; w = w->next)
    {
      WnckWindow *window = w->data;
      gchar     *desktop_file = NULL;
      
      desktop_file = wncksync_desktop_item_for_xid (wnck_window_get_xid (window));
      //try the next item
      if (!g_strcmp0("\0", desktop_file))
	      continue;
      // create our LauncherApplication by looking in the LauncherAppman
      bestmatch = launcher_appman_get_application_for_desktop_file (appman, 
                                                                    desktop_file);
      if (bestmatch)
        break;
    }
  
  /* If we have a match, just ref it, otherwise create an app to represent the
   * opened application */
  if (bestmatch)
    {
      g_object_ref (bestmatch);
    }
  else
    {
      bestmatch = launcher_appman_get_application_for_wnck_app (appman, 
                                                                app);
    }

  static GQuark quark;
  if (!quark)
    quark = g_quark_from_static_string ("launcher_app_qdata");

  launcher_application_add_wnckapp (bestmatch, app);
  g_object_set_qdata (G_OBJECT (app), quark, bestmatch);

  /* Add to the list of running apps, set the data property for the
   * WnckApplication class, so it's easy to locate when the application quits
   * and finally emit the signal notifying of the opened application
   */
  priv->running_apps = g_slist_append (priv->running_apps, bestmatch);
  g_object_set_data (G_OBJECT (app), LAUNCHERAPP_ID, bestmatch);
  g_signal_emit (session, _session_signals[APP_OPENED], 0, bestmatch);

  g_free (app_name);
  g_free (res_name);
  g_free (class_name);
}

static void
on_active_window_changed (WnckScreen *screen,
                          WnckWindow *previously_active_window,
                          LauncherSession *session)
{
  g_return_if_fail (session);
 
  WnckWindow *active_window = NULL;
  WnckApplication *new_wnckapp = NULL;
  LauncherApplication *old_focused_app = NULL;
  LauncherApplication *new_focused_app = NULL;

  static GQuark quark;
  if (!quark)
    quark = g_quark_from_static_string ("launcher_app_qdata");

  active_window = wnck_screen_get_active_window (screen); 

  if (previously_active_window)
    {
      WnckApplication *old_wnckapp = NULL;
      old_wnckapp = wnck_window_get_application (previously_active_window);
      if (WNCK_IS_APPLICATION (old_wnckapp))
        old_focused_app = g_object_get_qdata (G_OBJECT (old_wnckapp),
                                              quark);

      if (old_focused_app)
	      g_object_set (G_OBJECT (old_focused_app),
		      "focused", FALSE,
		      NULL);
    }
  

  
  if (active_window)
    { 
      new_wnckapp = wnck_window_get_application (active_window);
      new_focused_app = g_object_get_qdata (G_OBJECT (new_wnckapp),
					    quark);
      g_assert (LAUNCHER_IS_APPLICATION (new_focused_app));

      launcher_application_set_focused (new_focused_app, active_window);
    }
}


/*
 * Public Methods
 */

/**
 * launcher_session_get_running_applications:
 * @session: a #LauncherSession object
 *
 * This will produce a #GSList populated with currently running #LauncherApplication objects. 
 * This should be called after the mainloop has been started
 * <emphasis>This list should not be modified as it is owned by #LauncherSession</emphasis>
 *
 * Returns: A #GSList containing LauncherApplication objects
 */
 GSList *
launcher_session_get_running_applications (LauncherSession *session)
{
  g_return_val_if_fail (LAUNCHER_IS_SESSION (session), NULL);
  return session->priv->running_apps;
}
