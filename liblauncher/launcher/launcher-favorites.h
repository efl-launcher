/*
 * Copyright (C) 2009 Canonical, Ltd.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 * version 3.0 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3.0 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Authored by Neil Jagdish Patel <neil.patel@canonical.com>
 */

#ifndef _HAVE_LAUNCHER_FAVORITES_H
#define _HAVE_LAUNCHER_FAVORITES_H

#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS

#define LAUNCHER_TYPE_FAVORITES (launcher_favorites_get_type ())

#define LAUNCHER_FAVORITES(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
        LAUNCHER_TYPE_FAVORITES, LauncherFavorites))

#define LAUNCHER_FAVORITES_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), \
        LAUNCHER_TYPE_FAVORITES, LauncherFavoritesClass))

#define LAUNCHER_IS_FAVORITES(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
        LAUNCHER_TYPE_FAVORITES))

#define LAUNCHER_IS_FAVORITES_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), \
        LAUNCHER_TYPE_FAVORITES))

#define LAUNCHER_FAVORITES_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), \
        LAUNCHER_TYPE_FAVORITES, LauncherFavoritesClass))

typedef struct _LauncherFavorites LauncherFavorites;
typedef struct _LauncherFavoritesClass LauncherFavoritesClass;
typedef struct _LauncherFavoritesPrivate LauncherFavoritesPrivate;

struct _LauncherFavorites
{
  GObject         parent;

  /*< private >*/
  LauncherFavoritesPrivate   *priv;
};

struct _LauncherFavoritesClass
{
  GObjectClass    parent_class;

  /*< public >*/

  /*< private >*/
  void (*_launcher_favorites_1) (void);
  void (*_launcher_favorites_2) (void);
  void (*_launcher_favorites_3) (void);
  void (*_launcher_favorites_4) (void);
};

typedef enum
{
  LAUNCHER_FAVORITES_STRING,
  LAUNCHER_FAVORITES_INT,
  LAUNCHER_FAVORITES_FLOAT,
  LAUNCHER_FAVORITES_BOOL,
} LauncherFavoritesListValue;

GType      launcher_favorites_get_type         (void) G_GNUC_CONST;

/* Returns the LauncherFavorites singleton. Increments the ref_count, so make sure to
   g_object_unref it once your done.
 */
LauncherFavorites * launcher_favorites_get_default      (void);

GSList        * launcher_favorites_get_favorites    (LauncherFavorites *self);

void            launcher_favorites_set_favorites    (LauncherFavorites *self,
                                                     GSList            *list);

gboolean        launcher_favorites_add_favorite (LauncherFavorites *self,
                                                 const gchar       *uid);

gboolean        launcher_favorites_remove_favorite (LauncherFavorites *self,
                                                    const gchar       *uid);

gchar *         launcher_favorites_get_string (LauncherFavorites *self,
                                               const gchar       *uid,
                                               const gchar       *key);

void          launcher_favorites_set_string (LauncherFavorites *self,
                                             const gchar       *uid,
                                             const gchar       *key,
                                             const gchar       *value);

gint          launcher_favorites_get_int    (LauncherFavorites *self,
                                             const gchar       *uid,
                                             const gchar       *key);

void          launcher_favorites_set_int    (LauncherFavorites *self,
                                             const gchar       *uid,
                                             const gchar       *key,
                                             gint               value);

gfloat        launcher_favorites_get_float  (LauncherFavorites *self,
                                             const gchar       *uid,
                                             const gchar       *key);


void          launcher_favorites_set_float  (LauncherFavorites *self,
                                             const gchar       *uid,
                                             const gchar       *key,
                                             gfloat             value);

gboolean      launcher_favorites_get_bool   (LauncherFavorites *self,
                                             const gchar       *uid,
                                             const gchar       *key);

void          launcher_favorites_set_bool   (LauncherFavorites *self,
                                             const gchar       *uid,
                                             const gchar       *key,
                                             gboolean           value);

GSList      * launcher_favorites_get_list   (LauncherFavorites *self,
                                             const gchar       *uid,
                                             const gchar       *key,
                                         LauncherFavoritesListValue value_type);

void          launcher_favorites_set_list   (LauncherFavorites *self,
                                             const gchar       *uid,
                                             const gchar       *key,
                                         LauncherFavoritesListValue value_type,
                                             GSList            *value);

G_END_DECLS

#endif /* _HAVE_LAUNCHER_FAVORITES_H */
