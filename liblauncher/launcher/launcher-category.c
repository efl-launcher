/*
 * Copyright (C) 2009 Canonical, Ltd.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 * version 3.0 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3.0 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Authored by Neil Jagdish Patel <neil.patel@canonical.com>
 */
/**
 * SECTION:launcher-category
 * @short_description: Provides an interface to acquire data from #LauncherMenu 
 * categories
 * @include: launcher-category.h
 * 
 * #LauncherCategory provides methods of acquiring information about its 
 * children
 */
#if HAVE_CONFIG_H
#include <config.h>
#endif

#include "launcher-category.h"


enum
{
  PROP_0,

  PROP_NAME,
  PROP_ICON_NAME,
  PROP_COMMENT,
};

enum
{
  REMOVED,
  APPLICATION_ADDED,
  APPLICATION_REMOVED,

  LAST_SIGNAL
};


static guint category_signals[LAST_SIGNAL] = { 0 };

G_DEFINE_TYPE (LauncherCategory, launcher_category, G_TYPE_OBJECT);
#define LAUNCHER_CATEGORY_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE(obj, \
LAUNCHER_TYPE_CATEGORY, LauncherCategoryPrivate))

struct _LauncherCategoryPrivate
{
  gchar  *name;
  gchar  *comment;
  gchar  *icon_name;
  GSList *children;
};

static void
launcher_category_init (LauncherCategory *object)
{
    object->priv = LAUNCHER_CATEGORY_GET_PRIVATE (object);
}

static void
launcher_category_finalize (GObject *object)
{
  LauncherCategory *category = LAUNCHER_CATEGORY(object);
  LauncherCategoryPrivate *priv = category->priv;
  GSList *l;

  for (l = priv->children; l; l = l->next)
    g_object_unref(l->data);
    
  g_slist_free (priv->children);
  priv->children = NULL;

  G_OBJECT_CLASS (launcher_category_parent_class)->finalize (object);
}

static void
launcher_category_set_property (GObject *object, 
                                guint prop_id, 
                                const GValue *value, 
                                GParamSpec *pspec)
{
  LauncherCategory *category = LAUNCHER_CATEGORY(object);
  g_return_if_fail (LAUNCHER_IS_CATEGORY (category));
  LauncherCategoryPrivate *priv = category->priv;
  
  g_return_if_fail(priv);
    
  switch (prop_id)
    {
    case PROP_NAME:
      if (category->priv->name)
        g_free(category->priv->name);
      category->priv->name = g_value_dup_string(value);
      break;
    case PROP_ICON_NAME:
      if (category->priv->icon_name)
        g_free(category->priv->icon_name);
      category->priv->icon_name = g_value_dup_string(value);
      break;
    case PROP_COMMENT:
      if (category->priv->comment)
        g_free(category->priv->comment);
      category->priv->comment = g_value_dup_string(value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}


static void
launcher_category_get_property (GObject *object, 
                                guint prop_id, 
                                GValue *value, 
                                GParamSpec *pspec)
{
  LauncherCategory *category = LAUNCHER_CATEGORY(object);
  LauncherCategoryPrivate *priv = category->priv;
  
  g_return_if_fail (LAUNCHER_IS_CATEGORY (category));
  
  switch (prop_id)
    {
    case PROP_NAME:
      g_value_set_string(value, priv->name);
      break;
    case PROP_ICON_NAME:
      g_value_set_string(value, priv->icon_name);
      break;
    case PROP_COMMENT:
      g_value_set_string(value, priv->comment);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
launcher_category_class_init (LauncherCategoryClass *klass)
{
  GObjectClass* object_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (object_class, sizeof (LauncherCategoryPrivate));

  object_class->finalize = launcher_category_finalize;
  object_class->set_property = launcher_category_set_property;
  object_class->get_property = launcher_category_get_property;


  g_object_class_install_property (object_class,
                                   PROP_NAME,
                                   g_param_spec_string ("name",
                                                        "name",
                                                        "name of the category",
                                                        "",
                                                        G_PARAM_READABLE|G_PARAM_WRITABLE));

  g_object_class_install_property (object_class,
                                   PROP_ICON_NAME,
                                   g_param_spec_string ("icon-name",
                                                        "icon-name",
                                                        "the standard \"icon-name\" for this category",
                                                        "",
                                                        G_PARAM_READABLE|G_PARAM_WRITABLE));

  g_object_class_install_property (object_class,
                                   PROP_COMMENT,
                                   g_param_spec_string ("comment",
                                                        "comment",
                                                        "a general comment about the category, usually helpful information",
                                                        "",
                                                        G_PARAM_READABLE|G_PARAM_WRITABLE));

  category_signals[REMOVED] =
      g_signal_new ("removed",
                    G_OBJECT_CLASS_TYPE (klass),
                    0,
                    G_STRUCT_OFFSET (LauncherCategoryClass, removed),
                    NULL, NULL,
                    g_cclosure_marshal_VOID__POINTER,
                    G_TYPE_NONE, 1, G_TYPE_POINTER
                    );

  category_signals[APPLICATION_ADDED] =
      g_signal_new ("application-added",
                    G_OBJECT_CLASS_TYPE (klass),
                    0,
                    G_STRUCT_OFFSET (LauncherCategoryClass, application_added),
                    NULL, NULL,
                    g_cclosure_marshal_VOID__POINTER,
                    G_TYPE_NONE, 2, G_TYPE_POINTER, G_TYPE_POINTER
                    );
                    
  category_signals[APPLICATION_REMOVED] =
      g_signal_new ("application-removed",
                    G_OBJECT_CLASS_TYPE (klass),
                    0,
                    G_STRUCT_OFFSET (LauncherCategoryClass, application_removed),
                    NULL, NULL,
                    g_cclosure_marshal_VOID__POINTER,
                    G_TYPE_NONE, 2, G_TYPE_POINTER, G_TYPE_POINTER
                    );

}

/*
 * Public Methods
 */
/**
 * launcher_category_new:
 * @name: A string dictating the categorys name
 * @comment: A string comment on the category
 * @icon_name: a string refering to the icon_name of the category
 *
 * Creates a new #LauncherCategory with the given information
 *
 * Returns: a new #LauncherCategory
 */
LauncherCategory *
launcher_category_new (const gchar *name,
                       const gchar *comment,
                       const gchar *icon_name)
{
  LauncherCategory *category;

  category = g_object_new (LAUNCHER_TYPE_CATEGORY,
                           "name", name,
                           "comment", comment,
                           "icon_name", icon_name,
                           NULL);

  return category;
}

/**
 * launcher_category_get_name:
 * @category: A #LauncherCategory
 *
 * Provides the name information of @category
 * 
 * Returns: a string or NULL
 */
const gchar *
launcher_category_get_name (LauncherCategory *category)
{
  g_return_val_if_fail (category, NULL);
  return category->priv->name;
}

/**
 * launcher_category_get_comment:
 * @category: a #LauncherCategory
 * 
 * Provides the comment information of @category
 *
 * Returns: a string or NULL
 */
const gchar *
launcher_category_get_comment (LauncherCategory *category)
{
  g_return_val_if_fail (category, NULL);
  return category->priv->comment;
}

/**
 * launcher_category_get_icon_name:
 * @category: a #LauncherCategory
 *
 * Provides the icon name information of @category - can be used to create 
 * gtk/clutk icons
 *
 * Returns: a string or NULL
 */
 
const gchar *
launcher_category_get_icon_name (LauncherCategory *category)
{
  g_return_val_if_fail (category, NULL);
  return category->priv->icon_name;
}

/**
 * launcher_category_add_application:
 * @category: A #LauncherCategory
 * @application: a #LauncherApplication child
 *
 * Appends the given @application to the list of #LauncherApplication in 
 * @category
 */
void
launcher_category_add_application (LauncherCategory *category,
                                   LauncherApplication *application)
{
  g_return_if_fail (category);
  g_return_if_fail (application);

  category->priv->children = g_slist_append (category->priv->children, 
                                             application);
  g_object_ref(application);
  g_signal_emit (category, category_signals[APPLICATION_ADDED], 0, application);
}


/**
 * launcher_category_remove_application:
 * @category: A #LauncherCategory
 * @application: a #LauncherApplication child
 *
 * removes the given @application in the list of #LauncherApplication in 
 * @category
 */
void
launcher_category_remove_application (LauncherCategory *category,
                                      LauncherApplication *application)
{
  g_return_if_fail (category);
  g_return_if_fail (application);

  category->priv->children = g_slist_remove (category->priv->children, 
                                             application);
  g_object_unref(application);
  g_signal_emit (category, category_signals[APPLICATION_REMOVED], 
                 0, application);
}

/**
 * launcher_category_get_applications:
 * @category: a #LauncherCategory
 * 
 * Provides a list of children applications from @category
 *
 * Returns: A #GSList of #LauncherApplication or NULL
 */
GSList *
launcher_category_get_applications (LauncherCategory *category)
{
  g_return_val_if_fail (category, NULL);

  return category->priv->children;
}


/**
 * launcher_category_empty_applications:
 * @category: a #LauncherCategory
 * 
 * Provides a method in order to empty the category of all #LauncherApplication 
 * objects, 
 */
void
launcher_category_empty_applications (LauncherCategory *category)
{
  LauncherCategoryPrivate *priv = category->priv;
  GSList *l;
  
  g_return_if_fail (category);
  
  /* boy this is a lot of signals emited after a refresh... */
  /* FIXME - for the moment i am going to leave this as a fixme, 
   * at some point in the future I need to figure out a system (in 
   * launcher-menu.c) that is able to adaptivly figure out the difference
   * between a category before and after a refresh and add/remove applications
   * accordingly instead of just removing them all and then adding them all
   * again
   */
  for (l = priv->children; l; l = l->next)
    {
      g_object_unref(l->data);
      g_signal_emit (category, category_signals[APPLICATION_REMOVED], 
		     0, l->data);
    }

  g_slist_free (priv->children);
  priv->children = NULL;
}


/**
 * launcher_category_sort_applications:
 * @category: A #LauncherCategory
 * @sort_func: a #GCompareFunc function
 *
 * sorts the #LauncherApplication objects inside @category using the 
 * @sort_func provided
 */
void
launcher_category_sort_applications (LauncherCategory *category,
                                     GCompareFunc      sort_func)
{
  LauncherCategoryPrivate *priv = category->priv;
  
  g_return_if_fail (category);
  g_return_if_fail (sort_func);

  priv->children = g_slist_sort (priv->children, sort_func);
}
