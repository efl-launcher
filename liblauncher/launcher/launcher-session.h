/*
 * Copyright (C) 2009 Canonical, Ltd.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 * version 3.0 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3.0 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Authored by Neil Jagdish Patel <neil.patel@canonical.com>
 */

#ifndef _HAVE_LAUNCHER_SESSION_H
#define _HAVE_LAUNCHER_SESSION_H

#include <glib.h>
#include <glib-object.h>
#include <libwnck/libwnck.h>

G_BEGIN_DECLS

#define LAUNCHER_TYPE_SESSION (launcher_session_get_type ())

#define LAUNCHER_SESSION(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
        LAUNCHER_TYPE_SESSION, LauncherSession))

#define LAUNCHER_SESSION_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), \
        LAUNCHER_TYPE_SESSION, LauncherSessionClass))

#define LAUNCHER_IS_SESSION(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
        LAUNCHER_TYPE_SESSION))

#define LAUNCHER_IS_SESSION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), \
        LAUNCHER_TYPE_SESSION))

#define LAUNCHER_SESSION_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), \
        LAUNCHER_TYPE_SESSION, LauncherSessionClass))

typedef struct _LauncherSession LauncherSession;
typedef struct _LauncherSessionClass LauncherSessionClass;
typedef struct _LauncherSessionPrivate LauncherSessionPrivate;

struct _LauncherSession
{
  GObject         parent;

  /*< private >*/
  LauncherSessionPrivate   *priv;
};

struct _LauncherSessionClass
{
  GObjectClass    parent_class;

  /*< public >*/
  /* Session Application signals */
  void (*application_opened)    (LauncherSession     *session,
                                 LauncherApplication *application);
  void (*application_launching) (LauncherSession     *session,
                                 LauncherApplication *application);


  /*< private >*/
  void (*_launcher_session_1) (void);
  void (*_launcher_session_2) (void);
  void (*_launcher_session_3) (void);
  void (*_launcher_session_4) (void);
};

GType         launcher_session_get_type                 (void) G_GNUC_CONST;

/* Returns the LauncherSession singleton. Increments the ref_count, so make sure to * g_object_unref it once your done.
 */
LauncherSession * launcher_session_get_default              (void);

/* Get's a list of LauncherApplication structs. _DO NOT_ modify this list,
 * LauncherSession owns it. The LauncherApplication objects it contains can be
 * ref'd/unref'd as desired (although not needed in normal use)
 */
GSList      * launcher_session_get_running_applications (LauncherSession *session);

G_END_DECLS

#endif /* _HAVE_LAUNCHER_SESSION_H */
