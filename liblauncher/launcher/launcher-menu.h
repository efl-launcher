/*
 * Copyright (C) 2009 Canonical, Ltd.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 * version 3.0 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3.0 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Authored by Neil Jagdish Patel <neil.patel@canonical.com>
 */

#ifndef _HAVE_LAUNCHER_MENU_H
#define _HAVE_LAUNCHER_MENU_H

#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS

#define LAUNCHER_TYPE_MENU (launcher_menu_get_type ())

#define LAUNCHER_MENU(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
        LAUNCHER_TYPE_MENU, LauncherMenu))

#define LAUNCHER_MENU_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), \
        LAUNCHER_TYPE_MENU, LauncherMenuClass))

#define LAUNCHER_IS_MENU(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
        LAUNCHER_TYPE_MENU))

#define LAUNCHER_IS_MENU_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), \
        LAUNCHER_TYPE_MENU))

#define LAUNCHER_MENU_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), \
        LAUNCHER_TYPE_MENU, LauncherMenuClass))

typedef struct _LauncherMenu LauncherMenu;
typedef struct _LauncherMenuClass LauncherMenuClass;
typedef struct _LauncherMenuPrivate LauncherMenuPrivate;

struct _LauncherMenu
{
  GObject         parent;

  /*< private >*/
  LauncherMenuPrivate   *priv;
};

struct _LauncherMenuClass
{
  GObjectClass    parent_class;

  /*< public >*/
  void (*menu_changed) (LauncherMenu *menu);

  /*< private >*/
  void (*_launcher_menu_1) (void);
  void (*_launcher_menu_2) (void);
  void (*_launcher_menu_3) (void);
  void (*_launcher_menu_4) (void);
};

GType      launcher_menu_get_type         (void) G_GNUC_CONST;

/* Returns the LauncherMenu singleton. Increments the ref_count, so make sure to
   g_object_unref it once your done.
 */
LauncherMenu * launcher_menu_get_default      (void);

/* Returns a GSList of LauncherCategory pointers */
GSList   * launcher_menu_get_categories   (LauncherMenu *menu);

/* Returns a GSList of LauncherApplication pointers */
GSList   * launcher_menu_get_applications (LauncherMenu *menu);

G_END_DECLS

#endif /* _HAVE_LAUNCHER_MENU_H */
