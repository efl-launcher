/*
 * Copyright (C) 2009 Canonical, Ltd.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 * version 3.0 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3.0 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Authored by Neil Jagdish Patel <neil.patel@canonical.com>
 */

/**
 * SECTION:launcher-folder-bookmarks
 * @short_description: Holds a list of folder bookmarks
 * @includes: launcher-folder-bookmarks.h
 * 
 * #LauncherFolderBookmarks is a class that retrives and stores the users current folder bookmarks
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <gio/gio.h>

#include "launcher-folder-bookmarks.h"

#include "launcher-folder.h"

G_DEFINE_TYPE (LauncherFolderBookmarks, launcher_folder_bookmarks, G_TYPE_OBJECT)

#define LAUNCHER_FOLDER_BOOKMARKS_GET_PRIVATE(obj) \
    (G_TYPE_INSTANCE_GET_PRIVATE(obj, \
        LAUNCHER_TYPE_FOLDER_BOOKMARKS, LauncherFolderBookmarksPrivate))

#define BOOKMARKS_FILE ".gtk-bookmarks"

struct _LauncherFolderBookmarksPrivate
{
  GFile        *book_file;
  GFileMonitor *book_monitor;

  GSList *places;
};

enum
{
  BOOKMARKS_CHANGED,

  LAST_SIGNAL
};

static guint _bookmarks_signals[LAST_SIGNAL] = { 0 };

static LauncherFolderBookmarks *launcher_folder_bookmarks = NULL;

/* Forwards */
static void load_places          (LauncherFolderBookmarks *bookmarks);
static void on_bookmarks_changed (GFileMonitor        *monitor,
                                  GFile               *file,
                                  GFile               *old_file,
                                  GFileMonitorEvent    event,
                                  LauncherFolderBookmarks *bookmarks);

/* GObject Init */

static void
launcher_folder_bookmarks_finalize (GObject *folder_bookmarks)
{
  LauncherFolderBookmarksPrivate *priv;

  g_return_if_fail (LAUNCHER_IS_FOLDER_BOOKMARKS (folder_bookmarks));
  priv = LAUNCHER_FOLDER_BOOKMARKS (folder_bookmarks)->priv;

  g_slist_foreach (priv->places, (GFunc)launcher_folder_free, NULL);
  g_slist_free (priv->places);
  priv->places = NULL;

  if (G_IS_OBJECT (priv->book_file))
    {
      g_object_unref (priv->book_file);
      priv->book_file = NULL;
    }
  if (G_IS_OBJECT (priv->book_monitor))
    {
      g_object_unref (priv->book_monitor);
      priv->book_monitor = NULL;
    };

  G_OBJECT_CLASS (launcher_folder_bookmarks_parent_class)->finalize
  (folder_bookmarks);
}

static void
launcher_folder_bookmarks_class_init (LauncherFolderBookmarksClass *klass)
{
  GObjectClass *obj_class = G_OBJECT_CLASS (klass);

  obj_class->finalize = launcher_folder_bookmarks_finalize;

  _bookmarks_signals[BOOKMARKS_CHANGED] =
    g_signal_new ("bookmarks-changed",
                  G_OBJECT_CLASS_TYPE (obj_class),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (LauncherFolderBookmarksClass, bookmarks_changed),
                  NULL, NULL,
                  g_cclosure_marshal_VOID__VOID,
                  G_TYPE_NONE, 0);

  g_type_class_add_private (obj_class, sizeof (LauncherFolderBookmarksPrivate));
}

static void
launcher_folder_bookmarks_init (LauncherFolderBookmarks *bookmarks)
{
  LauncherFolderBookmarksPrivate *priv;
  gchar *file_name;
  GError *error = NULL;

  priv = bookmarks->priv = LAUNCHER_FOLDER_BOOKMARKS_GET_PRIVATE (bookmarks);

  priv->places = NULL;

  /* Monitor the bookmarks file for changes */
  file_name = g_build_filename (g_get_home_dir (), BOOKMARKS_FILE, NULL);

  priv->book_file = g_file_new_for_path (file_name);
  priv->book_monitor = g_file_monitor_file (priv->book_file, 0,
                       NULL, &error);
  if (error)
    {
      g_warning ("LauncherFolderBookmarks: Unable to monitor the bookmarks file '%s'"
                 ": %s", file_name, error->message);
      g_object_unref (priv->book_file);
      if (priv->book_monitor)
        g_object_unref (priv->book_monitor);
    }
  else
    {
      g_signal_connect (priv->book_monitor, "changed",
                        G_CALLBACK (on_bookmarks_changed), bookmarks);
    }
  g_free (file_name);
}

LauncherFolderBookmarks *
launcher_folder_bookmarks_get_default (void)
{
  if (G_UNLIKELY (!LAUNCHER_IS_FOLDER_BOOKMARKS (launcher_folder_bookmarks)))
    {
      launcher_folder_bookmarks = g_object_new (LAUNCHER_TYPE_FOLDER_BOOKMARKS,
                                  NULL);
      return launcher_folder_bookmarks;
    }

  return g_object_ref (launcher_folder_bookmarks);
}


/* Private methods */
static void
load_places (LauncherFolderBookmarks *bookmarks)
{
  LauncherFolderBookmarksPrivate *priv;
  gchar  *path = NULL;
  gchar  *contents = NULL;
  GSList *old_places;
  GError *error = NULL;

  g_return_if_fail (LAUNCHER_IS_FOLDER_BOOKMARKS (bookmarks));
  priv = bookmarks->priv;

  /* Although we are reloading the places, if possible, we don't want to do
   * unnecessary allocs/deallocs, so we save the current places, and try and
   * re-use them if possible
   */
  old_places = priv->places;
  priv->places = NULL;

  path = g_build_filename (g_get_home_dir (), ".gtk-bookmarks", NULL);

  if (!g_file_test (path, G_FILE_TEST_EXISTS))
    {
      g_warning ("LauncherFolderBookmarks: %s is not present. Cannot load folder "
                 "bookmarks", path);
      goto load_places_out;
    }

  if (!g_file_get_contents (path, &contents, NULL, &error))
    {
      g_warning ("LauncherFolderBookmarks: Unable to read contents of bookmarks "
                 "file: '%s'", error ? error->message : "Unknown");
      if (error)
        g_error_free (error);
      goto load_places_out;
    }

  if (contents)
    {
      gchar **entries;
      gint i;

      entries = g_strsplit (contents, "\n", -1);
      for (i = 0; entries[i]; i++)
        {
          LauncherFolder *folder = NULL;
          gchar  *name;
          gchar **tokens;

          tokens = g_strsplit (entries[i], " ", 2);

          if (tokens[0] == NULL)
            continue;

          if (tokens[1] != NULL)
            {
              name = tokens[1];
            }
          else
            {
              name = strrchr (tokens[0], '/');
              name++;
            }
          name = g_uri_unescape_string (name, "");

          /* If it's a local file, we need to make sure it actually exists */
          if (g_str_has_prefix (tokens[0], "file:"))
            {
              gchar *filepath = g_filename_from_uri (tokens[0], NULL, NULL);

              if (!g_file_test (filepath, G_FILE_TEST_EXISTS))
                {
                  g_free (name);
                  g_free (filepath);
                  g_strfreev (tokens);
                  continue;
                }

              g_free (filepath);
            }

          folder = launcher_folder_new (name, tokens[0]);
          if (folder)
            priv->places = g_slist_append (priv->places, folder);

          /*      g_debug ("New Bookmark: %s %s %s\n",
                         launcher_folder_get_name (folder),
                         launcher_folder_get_uri (folder),
                         launcher_folder_get_icon_name (folder));*/

          g_strfreev (tokens);
          g_free (name);
        }
      g_strfreev (entries);
    }

load_places_out:
  /* Free the old_places and signal the change */
  g_slist_foreach (old_places, (GFunc)launcher_folder_free, NULL);
  g_slist_free (old_places);
  g_free (path);
  g_free (contents);
}

/*
 * Called when the bookmarks file is changed (normally if the launcher has modified * the bookmarks in nautilus
 */
static void
on_bookmarks_changed (GFileMonitor        *monitor,
                      GFile               *file,
                      GFile               *old_file,
                      GFileMonitorEvent    event,
                      LauncherFolderBookmarks *bookmarks)
{
  g_return_if_fail (LAUNCHER_IS_FOLDER_BOOKMARKS (bookmarks));

  load_places (bookmarks);
  g_signal_emit (bookmarks, _bookmarks_signals[BOOKMARKS_CHANGED], 0);
}

/*
 * Public Methods
 */

/**
 * launcher_folder_bookmarks_get_bookmarks:
 * @bookmarks: a #LauncherFolderBookmarks object
 * 
 * Retrives the current list of folder bookmarks. 
 * This list is owned by the class so do not modify it.
 *
 * Returns: a GSList containing pointers to #LauncherFolder objects
 */
GSList *
launcher_folder_bookmarks_get_bookmarks (LauncherFolderBookmarks *bookmarks)
{
  g_return_val_if_fail (LAUNCHER_IS_FOLDER_BOOKMARKS (bookmarks), NULL);

  if (G_UNLIKELY (bookmarks->priv->places == NULL))
    load_places (bookmarks);

  return bookmarks->priv->places;
}
