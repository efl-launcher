/*
 * Copyright (C) 2009 Canonical, Ltd.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 * version 3.0 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3.0 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Authored by Neil Jagdish Patel <neil.patel@canonical.com>
 */

#ifndef _LAUNCHER_FOLDER_H_
#define _LAUNCHER_FOLDER_H_

#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS

#define LAUNCHER_TYPE_FOLDER	(launcher_folder_get_type ())

typedef struct _LauncherFolder LauncherFolder;

GType         launcher_folder_get_type      (void) G_GNUC_CONST;

LauncherFolder *  launcher_folder_new           (const gchar  *name,
                                         const gchar  *uri);

void          launcher_folder_free          (LauncherFolder *folder);

const gchar * launcher_folder_get_name      (LauncherFolder *folder);

const gchar * launcher_folder_get_uri       (LauncherFolder *folder);

const gchar * launcher_folder_get_icon_name (LauncherFolder *folder);

/*
 * Will launch the correct application for the folder, mounting the volume if
 * necessary
 */
void          launcher_folder_activate      (LauncherFolder *folder, GError **error);

G_END_DECLS

#endif /* _LAUNCHER_FOLDER_H_ */
