/*
 * Copyright (C) 2009 Canonical, Ltd.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 * version 3.0 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3.0 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Authored by Neil Jagdish Patel <neil.patel@canonical.com>
 */

#ifndef _LAUNCHER_CATEGORY_H_
#define _LAUNCHER_CATEGORY_H_

#include <glib.h>
#include <glib-object.h>
#include <gdk/gdk.h>
#include "launcher-application.h"
G_BEGIN_DECLS

#define LAUNCHER_TYPE_CATEGORY (launcher_category_get_type ())

#define LAUNCHER_CATEGORY(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
        LAUNCHER_TYPE_CATEGORY, LauncherCategory))
        
#define LAUNCHER_CATEGORY_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), \
        LAUNCHER_TYPE_CATEGORY, LauncherCategoryClass))
        
#define LAUNCHER_IS_CATEGORY(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
        LAUNCHER_TYPE_CATEGORY))
        
#define LAUNCHER_IS_CATEGORY_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), \
        LAUNCHER_TYPE_CATEGORY))
        
#define LAUNCHER_CATEGORY_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), \
        LAUNCHER_TYPE_CATEGORY, LauncherCategoryClass))
        
typedef struct _LauncherCategory LauncherCategory;
typedef struct _LauncherCategoryClass LauncherCategoryClass;
typedef struct _LauncherCategoryPrivate LauncherCategoryPrivate;

struct _LauncherCategory
{
  GObject parent_instance;

  /* Private */
  LauncherCategoryPrivate *priv;
};


struct _LauncherCategoryClass
{
  GObjectClass parent_class;

  /* Signals */
  void(* removed)               (LauncherCategory *self);
  void(* application_added)     (LauncherCategory *self, 
                                 LauncherApplication *application);
  void(* application_removed)   (LauncherCategory *self, 
                                 LauncherApplication *application);
};





LauncherCategory * launcher_category_new   (const gchar *name,
                                            const gchar *comment,
                                            const gchar *icon_name);
                                            
const gchar *  launcher_category_get_name      (LauncherCategory *category);

const gchar *  launcher_category_get_comment   (LauncherCategory *category);

const gchar *  launcher_category_get_icon_name (LauncherCategory *category);

void           launcher_category_add_application     (LauncherCategory *category,
                                                      LauncherApplication *application);
                                                      
void           launcher_category_remove_application  (LauncherCategory *category,
                                                      LauncherApplication *application);

GSList      *  launcher_category_get_applications    (LauncherCategory *category);

void           launcher_category_empty_applications  (LauncherCategory *category);

void           launcher_category_sort_applications   (LauncherCategory *category,
                                                      GCompareFunc      sort_func);

G_END_DECLS

#endif /* _LAUNCHER_CATEGORY_H_ */
