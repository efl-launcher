/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * Copyright (C) 2009 Canonical, Ltd.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 * version 3.0 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3.0 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Authored by Gordon Allott (gord.allott@canonical.com)
 */
#include "launcher-application.h"
#include "launcher-appman.h"

/**
 * SECTION:launcher-appman 
 * @short_description: #LauncherAppman is a singleton object designed to cache 
 * and hold LauncherApplication objects
 * @include: launcher-appman.h
 *
 * #LauncherAppman is a singleton that will serve as a single point of entry  
 * for creating #LauncherApplication objects, all #LauncherApplication objects 
 * should be requested from this cache by providing a desktop file to the 
 * application you wish to create.
 */

G_DEFINE_TYPE (LauncherAppman, launcher_appman, G_TYPE_OBJECT);
#define LAUNCHER_APPMAN_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE(obj, \
LAUNCHER_TYPE_APPMAN, LauncherAppmanPrivate))

struct _LauncherAppmanPrivate
{
  GHashTable *app_lookup;
  GSequence *app_list;
};

static LauncherAppman *launcher_appman = NULL;


static void
launcher_appman_finalize (GObject *appman)
{
  LauncherAppmanPrivate *priv;
  g_return_if_fail (LAUNCHER_IS_APPMAN (appman));
  priv = LAUNCHER_APPMAN (appman)->priv;

  /* we assume that the application is shutting down and no longer needs the 
   * app cache 
   */
  g_hash_table_destroy(priv->app_lookup);
  g_sequence_free(priv->app_list);
  
  G_OBJECT_CLASS (launcher_appman_parent_class)->finalize (appman);
}

static void
launcher_appman_init (LauncherAppman *appman)
{
  LauncherAppmanPrivate *priv;
  
  priv = appman->priv = LAUNCHER_APPMAN_GET_PRIVATE (appman);
  /* FIXME - replace with g_hash_table_new_full */
  priv->app_lookup = g_hash_table_new(g_str_hash, g_str_equal); 
  /* FIXME - add GDestroyNotify peramater */
  priv->app_list = g_sequence_new(NULL); 
}


static void
launcher_appman_class_init (LauncherAppmanClass *klass)
{
  GObjectClass* object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = launcher_appman_finalize;
  g_type_class_add_private (object_class, sizeof (LauncherAppmanPrivate));
}

/*
 * Public methods
 */

/**
 * launcher_appman_get_default
 * 
 * Returns the default LauncherAppman singleton
 * 
 * Returns: A #LauncherApppman Object
 */
LauncherAppman *
launcher_appman_get_default (void)
{
  if (G_UNLIKELY (!LAUNCHER_IS_APPMAN (launcher_appman)))
    {
      launcher_appman = g_object_new (LAUNCHER_TYPE_APPMAN,
                                       NULL);
      return launcher_appman;
    }

  return g_object_ref (launcher_appman);
}

/**
 * launcher_appman_get_application_for_desktop_file
 * @appman: A #LauncherAppman object
 * @desktop: A String path to the desktop file
 * 
 * Provides a method for accessing #LauncherApplication objects by providing 
 * a desktop file
 * 
 * Returns: A LauncherApplication object or NULL
 */
LauncherApplication *
launcher_appman_get_application_for_desktop_file (LauncherAppman *appman, 
                                                  const gchar *desktop)
{
  LauncherApplication *found_app;

  g_return_val_if_fail (LAUNCHER_IS_APPMAN (appman), NULL);

  /* try and find the application in our cache */
  found_app = g_hash_table_lookup (appman->priv->app_lookup, desktop);

  if (!found_app) 
    {
      /* we don't have this app in the cache yet, we need to generate it first
       */    
      found_app = launcher_application_new_from_desktop_file (desktop);
      if (found_app != NULL)
      {
        //add our app to the hash table
        g_sequence_append (appman->priv->app_list, found_app);
        g_hash_table_insert (appman->priv->app_lookup, 
                            g_strdup (desktop), 
                            found_app);
        
      } else {
        // if we get here, there is a problem with the desktop file
        g_critical("Could not create desktop file from %s", desktop);
      }
    }
    
  return found_app;
}

/**
 * launcher_appman_get_application_for_wnck_app
 * @appman: A #LauncherAppman object
 * @name: A WnckApplication 
 * 
 * Provides a method for accessing #LauncherApplication objects by providing 
 * a WnckApplication object
 * 
 * Returns: A LauncherApplication object or NULL
 */
LauncherApplication *
launcher_appman_get_application_for_wnck_app (LauncherAppman *appman,
                                              WnckApplication *wnck_app)
{
  LauncherApplication *   found_app;
  const gchar *           wnck_name;
  
  g_return_val_if_fail (LAUNCHER_IS_APPMAN (appman), NULL);
  g_return_val_if_fail (WNCK_IS_APPLICATION (wnck_app), NULL);
  // we need this method because of complications todo with sometimes not having
  // a desktop file available
  
  wnck_name = wnck_application_get_name (wnck_app);
  
  found_app = g_hash_table_lookup (appman->priv->app_lookup, wnck_name);
  if (!found_app)
    {
      /* we don't have an app with this name in the cache yet, so generate a new
       * one, this app basically has no info though
       */
      found_app = launcher_application_new_from_wnck_app (wnck_app);
      if (found_app != NULL)
        {
          //add our app to the hash table
          g_sequence_append (appman->priv->app_list, found_app);
          g_hash_table_insert (appman->priv->app_lookup, 
                               g_strdup (wnck_name), 
                               found_app);
        }
    }
  
  return found_app; 
}
                                            

/**
 * launcher_appman_get_applications
 * @appman: a #LauncherAppman object
 * 
 * returns a #GSequence object that contains all the applications in
 * the current cache
 * 
 * Returns: (transfer none): An unowned #GSequence of #LauncherApplication
 * objects
 */
GSequence *
launcher_appman_get_applications (LauncherAppman *appman)
{  
  g_return_val_if_fail (LAUNCHER_IS_APPMAN (appman), NULL);
  return appman->priv->app_list;
}
