/*
 * Copyright (C) 2009 Canonical, Ltd.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 * version 3.0 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3.0 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Authored by Neil Jagdish Patel <neil.patel@canonical.com>
 */
/**
 * SECTION:launcher-icon-utils
 * @short_description: Provides utilities for working with icons
 *
 * Provides utilities for working with icons
 */
#if HAVE_CONFIG_H
#include <config.h>
#endif

#include <gio/gio.h>
#include <gtk/gtk.h>

#include "launcher-icon-utils.h"

/**
 * launcher_icon_utils_icon_name_for_volume_uri:
 * @uri: a string containing the URI of the volume
 *
 * Given @uri will return an icon_name string associated with the volume
 *
 * Returns: An icon_name string
 */
gchar *
launcher_icon_utils_icon_name_for_volume_uri (const gchar *uri)
{
  GFile               *file = NULL;
  GFileInfo           *file_info = NULL;
  GIcon               *icon = NULL;
  const gchar * const *icon_names;
  gchar               *icon_name = NULL;
  static GtkIconTheme *icon_theme = NULL;
  GError              *error = NULL;
  int                  i;

  if (uri == NULL)
    goto launcher_icon_utils_icon_name_for_volume_uri_out;

  /* This is safe because we don't own the icon theme object, it finalises
   * itself at termination
   */
  if (icon_theme == NULL)
    icon_theme = gtk_icon_theme_get_default ();

  file = g_file_new_for_uri (uri);
  file_info = g_file_query_info (file, G_FILE_ATTRIBUTE_STANDARD_ICON, 0,
                                 NULL, &error);

  if (error || !file_info)
    {
      if (error)
        g_error_free (error);

      goto launcher_icon_utils_icon_name_for_volume_uri_out;
    }

  icon = (GIcon *)g_file_info_get_attribute_object (file_info,
         G_FILE_ATTRIBUTE_STANDARD_ICON);

  if (!G_IS_THEMED_ICON (icon))
    {
      goto launcher_icon_utils_icon_name_for_volume_uri_out;
    }

  icon_names = g_themed_icon_get_names (G_THEMED_ICON (icon));
  for (i = 0; icon_names[i]; i++)
    {
      if (gtk_icon_theme_has_icon (icon_theme, icon_names[i]))
        {
          icon_name = g_strdup (icon_names[i]);
          break;
        }
    }

launcher_icon_utils_icon_name_for_volume_uri_out:
  if (G_IS_OBJECT (file_info))
    g_object_unref (file_info);
  if (G_IS_FILE (file))
    g_object_unref (file);

  return icon_name ? icon_name : g_strdup ("folder");
}
