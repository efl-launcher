/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * Copyright (C) 2009 Canonical, Ltd.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 * version 3.0 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3.0 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Authored by Neil Jagdish Patel <neil.patel@canonical.com>
 */

/**
 * SECTION:launcher-application 
 * @short_description: #LauncherApplication objects abstract application handling
 * @include: launcher-application.h
 *
 * #LauncherApplication objects exist in order to acquire data about specific 
 * applications and to launch applications. it also provides a method for 
 * returning a libwnck application
 */
#if HAVE_CONFIG_H
#include <config.h>
#endif

#include "launcher-application.h"

#include <gio/gdesktopappinfo.h>

#define TYPE_GS_LIST gs_list_get_type()

static gpointer gs_list_copy (gpointer boxed)
{
  return g_memdup (boxed, sizeof (GSList));
}

GType gs_list_get_type (void)
{
  static GType type = 0;

  if (!type) {
    type = g_boxed_type_register_static ("GSList", 
       gs_list_copy, g_free);
  }

  return type;
}

enum
{
  PROP_0,

  PROP_NAME,
  PROP_EXEC,
  PROP_ICON_NAME,
  PROP_COMMENT,
  PROP_DESKTOP_FILE_PATH,
  PROP_UNIQUE_STRING,
  PROP_CATEGORIES,
  PROP_RUNNING,
  PROP_FAVORITE,
  PROP_FOCUSED,
  PROP_WNCKAPPLICATIONS
};

enum
{
  OPENED,
  CLOSED,

  LAST_SIGNAL
};

static guint application_signals[LAST_SIGNAL] = { 0 };

G_DEFINE_TYPE (LauncherApplication, launcher_application, G_TYPE_OBJECT);
#define LAUNCHER_APPLICATION_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE(obj, \
LAUNCHER_TYPE_APPLICATION, LauncherApplicationPrivate))

struct _LauncherApplicationWindow
{
  GTimeVal timestamp;
  WnckWindow *window;
};

struct _LauncherApplicationPrivate
{
  gchar *name;
  gchar *exec;
  gchar *icon_name;
  gchar *comment;
  gchar *desktop_file_path;
  gchar *unique_string;
  GSList *categories;
  gboolean running;
  gboolean favorite;
  GSList *wnck_apps;
  gboolean focused;
  
  WnckScreen  *screen;
  GHashTable *windows;
};

static void
on_application_closed (WnckScreen              *screen,
                       WnckApplication         *app,
                       LauncherApplication     *application);

static void
launcher_application_init (LauncherApplication *object)
{
  LauncherApplicationPrivate *priv;
    
  priv = object->priv = LAUNCHER_APPLICATION_GET_PRIVATE (object);
  priv->running = FALSE;
  priv->favorite = FALSE;
  
  priv->screen = wnck_screen_get_default ();
  g_signal_connect (priv->screen, "application-closed",
                    G_CALLBACK (on_application_closed), object);

  priv->windows = g_hash_table_new (g_direct_hash, g_direct_equal);
}

static void
launcher_application_finalize (GObject *object)
{
  LauncherApplication *app = LAUNCHER_APPLICATION (object);
  LauncherApplicationPrivate *priv;
  
  priv = LAUNCHER_APPLICATION_GET_PRIVATE (app);
  g_hash_table_destroy (priv->windows);
  
  G_OBJECT_CLASS (launcher_application_parent_class)->finalize (object);
  
}

static void
launcher_application_set_property (GObject *object, 
                                   guint prop_id, 
                                   const GValue *value, 
                                   GParamSpec *pspec)
{
  LauncherApplication *application = LAUNCHER_APPLICATION(object);
  g_return_if_fail (LAUNCHER_IS_APPLICATION (application));
  
    
  switch (prop_id)
    {
    case PROP_NAME:
      if (application->priv->name)
        g_free(application->priv->name);
      application->priv->name = g_value_dup_string(value);
      break;
    case PROP_EXEC:
      if (application->priv->exec)
        g_free(application->priv->exec);
      application->priv->exec = g_value_dup_string(value);
      break;
    case PROP_ICON_NAME:
      if (application->priv->icon_name)
        g_free(application->priv->icon_name);
      application->priv->icon_name = g_value_dup_string(value);
      break;
    case PROP_COMMENT:
      if (application->priv->comment)
        g_free(application->priv->comment);
      application->priv->comment = g_value_dup_string(value);
      break;
    case PROP_DESKTOP_FILE_PATH:
      launcher_application_set_desktop_file(application, 
                                            g_value_dup_string(value));
      break;
    case PROP_UNIQUE_STRING:
      break;
    case PROP_CATEGORIES:
      application->priv->categories = g_value_get_boxed(value);
      break;
    case PROP_RUNNING:
      application->priv->running = g_value_get_boolean(value);
      break;
    case PROP_FAVORITE:
      application->priv->favorite = g_value_get_boolean(value);
      break;
    case PROP_WNCKAPPLICATIONS:
      application->priv->wnck_apps = g_value_get_boxed(value);
      break;
    case PROP_FOCUSED:
      application->priv->focused = g_value_get_boolean (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
launcher_application_get_property (GObject *object, 
                                   guint prop_id, 
                                   GValue *value, 
                                   GParamSpec *pspec)
{
  LauncherApplication *application = LAUNCHER_APPLICATION(object);
  LauncherApplicationPrivate *priv = application->priv;
  
  g_return_if_fail (LAUNCHER_IS_APPLICATION (application));
  
  switch (prop_id)
    {
    case PROP_NAME:
      g_value_set_string(value, priv->name);
      break;
    case PROP_EXEC:
      g_value_set_string(value, priv->exec);
      break;
    case PROP_ICON_NAME:
      g_value_set_string(value, priv->icon_name);
      break;
    case PROP_COMMENT:
      g_value_set_string(value, priv->comment);
      break;
    case PROP_DESKTOP_FILE_PATH:
      g_value_set_string(value, priv->desktop_file_path);
      break;
    case PROP_UNIQUE_STRING:
      {
        const gchar *str;
        str = launcher_application_get_unique_string(application);
        g_value_set_string(value, str);
      }
      break;
    case PROP_CATEGORIES:
      g_value_set_boxed(value, &priv->categories);
      break;
    case PROP_RUNNING:
      g_value_set_boolean(value, priv->running);
      break;
    case PROP_FAVORITE:
      g_value_set_boolean(value, priv->favorite);
      break;
    case PROP_WNCKAPPLICATIONS:
      g_value_set_object(value, priv->wnck_apps);
      break;
    case PROP_FOCUSED:
      {
        gboolean focused = FALSE;
        focused = launcher_application_get_focused (application);
        g_value_set_boolean (value, priv->focused);
      }
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
launcher_application_opened (LauncherApplication *self, 
                             WnckApplication *wnckapp)
{
  /* TODO: Add default signal handler implementation here */
}

static void
launcher_application_closed (LauncherApplication *self,
                             WnckApplication *wnckapp)
{
  /* TODO: Add default signal handler implementation here */
}

static void
launcher_application_class_init (LauncherApplicationClass *klass)
{
  GObjectClass* object_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (object_class, sizeof (LauncherApplicationPrivate));

  object_class->finalize = launcher_application_finalize;
  object_class->set_property = launcher_application_set_property;
  object_class->get_property = launcher_application_get_property;

  klass->opened = launcher_application_opened;
  klass->closed = launcher_application_closed;

  g_object_class_install_property (object_class,
                                   PROP_NAME,
                                   g_param_spec_string ("name",
                                                        "name",
                                                        "name of the application",
                                                        "",
                                                        G_PARAM_READABLE));

  g_object_class_install_property (object_class,
                                   PROP_EXEC,
                                   g_param_spec_string ("exec",
                                                        "exec",
                                                        "the exec thats called in order to launch the application",
                                                        "",
                                                        G_PARAM_READABLE));

  g_object_class_install_property (object_class,
                                   PROP_ICON_NAME,
                                   g_param_spec_string ("icon-name",
                                                        "icon-name",
                                                        "the standard \"icon-name\" for this application",
                                                        "",
                                                        G_PARAM_READABLE));

  g_object_class_install_property (object_class,
                                   PROP_COMMENT,
                                   g_param_spec_string ("comment",
                                                        "comment",
                                                        "a general comment about the application, usually helpful information",
                                                        "",
                                                        G_PARAM_READABLE));

  g_object_class_install_property (object_class,
                                   PROP_DESKTOP_FILE_PATH,
                                   g_param_spec_string ("desktop_file_path",
                                                        "desktop_file_path",
                                                        "the path to the desktop file accociated with this application",
                                                        "",
                                                        G_PARAM_READABLE|G_PARAM_WRITABLE));

  g_object_class_install_property (object_class,
                                   PROP_UNIQUE_STRING,
                                   g_param_spec_string ("unique_string",
                                                        "unique_string",
                                                        "A Unique string to identify this application",
                                                        "",
                                                        G_PARAM_READABLE));
  g_object_class_install_property (object_class,
                                   PROP_CATEGORIES,
                                   g_param_spec_boxed  ("categories",
                                                        "categories",
                                                        "a list of LauncherCategories that this application is listed inside",
                                                        TYPE_GS_LIST,
                                                        G_PARAM_READABLE));

  g_object_class_install_property (object_class,
                                   PROP_RUNNING,
                                   g_param_spec_boolean ("running",
                                                        "running",
                                                        "Returns TRUE if the application is running",
                                                        FALSE,
                                                        G_PARAM_READABLE|G_PARAM_WRITABLE));

  g_object_class_install_property (object_class,
                                   PROP_FOCUSED,
                                   g_param_spec_boolean ("focused",
                                                         "focused",
                                                         "returns TRUE if the application contains a focused window",
                                                         FALSE,
                                                         G_PARAM_READABLE|G_PARAM_WRITABLE));

  g_object_class_install_property (object_class,
                                   PROP_FAVORITE,
                                   g_param_spec_boolean ("favorite",
                                                        "favorite",
                                                        "returns TRUE if the application is listed as a favorite application",
                                                        FALSE,
                                                        G_PARAM_READABLE));

  g_object_class_install_property (object_class,
                                   PROP_WNCKAPPLICATIONS,
                                   g_param_spec_boxed  ("wnckapps",
                                                        "wnckapps",
                                                        "a list of WnckApplication objects that this Application current has",
                                                        TYPE_GS_LIST,
                                                        G_PARAM_READABLE));

  application_signals[OPENED] =
      g_signal_new ("opened",
                    G_OBJECT_CLASS_TYPE (klass),
                    0,
                    G_STRUCT_OFFSET (LauncherApplicationClass, opened),
                    NULL, NULL,
                    g_cclosure_marshal_VOID__POINTER,
                    G_TYPE_NONE, 1, G_TYPE_POINTER
                    );

  application_signals[CLOSED] =
      g_signal_new ("closed",
                    G_OBJECT_CLASS_TYPE (klass),
                    0,
                    G_STRUCT_OFFSET (LauncherApplicationClass, closed),
                    NULL, NULL,
                    g_cclosure_marshal_VOID__POINTER,
                    G_TYPE_NONE, 1, WNCK_TYPE_APPLICATION
                    );
}

/*
 * Private Methods
 */


static void
on_application_closed (WnckScreen      *screen,
                       WnckApplication *app,
                       LauncherApplication     *application)
{
  int       wnck_pid = 0;
  int       app_pid = 1;
  GSList    *a;
  WnckApplication *found_app = NULL;
  /* when *any* application closes we do a quick check to see if its this one
   * thus we need to make this check as quick and easy as possible
   */
  g_return_if_fail (LAUNCHER_IS_APPLICATION (application));
  g_return_if_fail (WNCK_IS_APPLICATION (app));
  
  /* we need to go though and check each wnckapp in this launcherapplication 
   * to see if the pid's match
   */
  wnck_pid = wnck_application_get_pid (app);
  for (a = application->priv->wnck_apps; a; a = a->next) 
    {
      WnckApplication *store_app = a->data;
      app_pid = wnck_application_get_pid (store_app);
      if (wnck_pid == app_pid) 
        {
          found_app = store_app;
          break;
        }
    }
    
  if (!found_app)
    return;
    
  // we get here then we have the wnckapplication in our store
  application->priv->wnck_apps = g_slist_remove(application->priv->wnck_apps,
                                                found_app);
  g_object_unref (found_app);
  
  // do we have any apps in our store? if so, we are running!
  if (application->priv->wnck_apps != NULL)
    {
      g_object_set (G_OBJECT (application),
                    "running", TRUE,
                    NULL);
    } else
    { 
      g_object_set (G_OBJECT (application),
                    "running", FALSE,
                    NULL);
    }   
    
  g_object_set (G_OBJECT (application),
                "focused", FALSE,
                NULL);

  // we are closing apprently, lets emit a signal about that :-)
  g_signal_emit (application, application_signals[CLOSED], 0, app);
}



/* 
 * Constructors 
 */


LauncherApplication *
launcher_application_new (void)
{
  LauncherApplication *application;

  application = g_object_new (LAUNCHER_TYPE_APPLICATION,
                              "running", FALSE,
                              NULL);
  return application;
}

/**
 * launcher_application_new_from_wnck_app:
 * @app: A #WnckApplication object
 *
 * creates a new #LauncherApplication object based on information from @app
 */
LauncherApplication *
launcher_application_new_from_wnck_app (WnckApplication *app)
{
  LauncherApplication *application;

  application = g_object_new (LAUNCHER_TYPE_APPLICATION,
                        "running", TRUE,
                        NULL);
  launcher_application_add_wnckapp (application, app);
  return application;
}

/**
 * launcher_application_new_from_destkop_file
 * @desktop_file: the file path to the desktop file used to build the application
 * 
 * This will create a new #LauncherApplication object using the information
 * contained within the given desktop file at @desktop_file
 * 
 * Returns: (transfer full) A new #LauncherApplication object
 */
LauncherApplication *
launcher_application_new_from_desktop_file (const gchar *desktop_file)
{
  LauncherApplication *application;
  /* we can now make our application */
  application = g_object_new (LAUNCHER_TYPE_APPLICATION,
                              "desktop_file_path", desktop_file, 
                              "running", FALSE,
                              NULL);

  return application;
}

/*
 * Public Methods
 */

/**
 * launcher_application_launch
 * @application: a #LauncherApplication object
 * @error: a #GError error
 * 
 * This method will attempt to launch the application using the exec string
 * associated with it
 * 
 * Returns: A truth value dictating whether the launching was successful
 */
gboolean
launcher_application_launch (LauncherApplication *application, GError **error)
{
  GDesktopAppInfo     *info;
  gchar               *icon_name;
  gchar               *desktop_file_path;
  GdkAppLaunchContext *context;

  g_return_val_if_fail (application, FALSE);
  g_return_val_if_fail (*error == NULL, FALSE);
  
  g_object_get(application, "desktop_file_path", &desktop_file_path, NULL);

  info = g_desktop_app_info_new_from_filename (desktop_file_path);

  if (!info)
    {
      static GQuark quark;
      if (!quark)
        quark = g_quark_from_static_string ("launcher_application_launch_error");

      *error = g_error_new (quark, 1, "Unable to load GDesktopAppInfo for %s",
                            desktop_file_path);
      return FALSE;
    }

  context = gdk_app_launch_context_new ();
  gdk_app_launch_context_set_screen (context, gdk_screen_get_default ());
  gdk_app_launch_context_set_timestamp (context, GDK_CURRENT_TIME);
  
  g_object_get(application, "icon-name", &icon_name, NULL);
  gdk_app_launch_context_set_icon_name (context, icon_name);

  g_app_info_launch ((GAppInfo *)info, NULL, (GAppLaunchContext*)context,
                     error);

  g_object_unref (context);
  g_object_unref (info);

  return TRUE;
}

/**
 * launcher_application_get_unique_string
 * @app: a #LauncherApplication
 * 
 * Provides a unique string that can be used to identify this application
 *
 * Returns: a unique string
 */
const gchar *
launcher_application_get_unique_string (LauncherApplication *application)
{
  g_return_val_if_fail (application, NULL);
  if (!application->priv->unique_string)
    application->priv->unique_string = g_utf8_strdown (application->priv->name, 
                                                       -1);
                                                       
  return application->priv->unique_string;
}

/**
 * launcher_application_get_wnckapp:
 * @app: a #LauncherApplication
 * 
 * Provides a list of #WnckApplications associated with this @app
 * 
 * Returns: (transfer none): a #GSList containing #WnckApplications 
 */
GSList *
launcher_application_get_wnckapps (LauncherApplication *application)
{
  g_return_val_if_fail (application, NULL);
  
  return application->priv->wnck_apps;
}

/**
 * launcher_application_add_wnckapp
 * @application: a #LauncherApplication object
 * @wnck_app: a #WnckApplication object
 * 
 * This method will add @wnck_app to @application and associate it with
 * the various signals @application has.
 */ 
void
launcher_application_add_wnckapp (LauncherApplication *application, 
                                  WnckApplication *wnck_app) 
{
  GSList *a;
  gint pid = 0;
  g_return_if_fail (application);
  g_return_if_fail (wnck_app);
  
  /* first i guess we need to check to make sure that
   * this WnckApplication does not already exist in the list
   * of wnckapplications
   */
  pid = wnck_application_get_pid (wnck_app);
  for (a = application->priv->wnck_apps; a; a = a->next)
    {
      WnckApplication *app = a->data;
      if (wnck_application_get_pid (app) == pid)
        return;
    }

  application->priv->wnck_apps = g_slist_append(application->priv->wnck_apps, 
                                                wnck_app);
  g_object_ref(wnck_app);
  
  // we emit the opened signal here
  g_signal_emit (application, application_signals[OPENED], 0, wnck_app);
  
  g_object_set (G_OBJECT (application),
                "running", TRUE,
                NULL);

}
  
/**
 * launcher_application_get_name:
 * @application: a #LauncherApplication object
 *
 * Retrives the name of the application @application
 *
 * Returns: a string containing the name
 */
const gchar *
launcher_application_get_name (LauncherApplication *application)
{
  g_return_val_if_fail (application, NULL);
  
  return application->priv->name;
}

/**
 * launcher_application_get_comment:
 * @application: A #LauncherApplication object
 *
 * Retrives the comment assigned to the given @application object
 *
 * Returns: A string containing the comment
 */
const gchar *
launcher_application_get_comment (LauncherApplication *application)
{
  g_return_val_if_fail (application, NULL);
  
  return application->priv->comment;
}

/**
 * launcher_application_get_icon_name
 * @application: A #LauncherApplication object
 * 
 * provides an icon-name associated with the application
 * 
 * Returns: an icon-name string
 */
const gchar *
launcher_application_get_icon_name (LauncherApplication *application)
{
  g_return_val_if_fail (application, NULL);
  
  return application->priv->icon_name;
}

/**
 * launcher_application_get_exec_string
 * @application: A #LauncherApplication
 * 
 * provides the executionable string for the application
 * 
 * Returns: a string representing the excutionable string
 */
const gchar *
launcher_application_get_exec_string (LauncherApplication *application)
{
  g_return_val_if_fail (application, NULL);
  
  return application->priv->exec;
}

/**
 * launcher_application_get_desktop_file
 * @application: A #LauncherApplication
 * 
 * provides the desktop file path for the application
 * 
 * Returns: a string that contains the path to the desktop file for this application
 */
const gchar *
launcher_application_get_desktop_file (LauncherApplication *application)
{
  g_return_val_if_fail (application, NULL);
  
  return application->priv->desktop_file_path;
}

/**
 * launcher_application_set_desktop_file_path
 * @application: A #LauncherApplication
 * @desktop_file: A string describing the path to a desktop file
 * 
 * sets the desktop file for this application and will also regenerate its 
 * information
 * 
 */
void
launcher_application_set_desktop_file (LauncherApplication *application,
                                       gchar *desktop_file)
{
  GKeyFile *desktop_keyfile;
  GError    *error = NULL;
  gchar     *name = NULL;
  gchar     *exec = NULL;
  gchar     *comment = NULL;
  gchar     *icon_name = NULL;
  
  g_return_if_fail (application);
  g_return_if_fail (desktop_file);

  desktop_keyfile = g_key_file_new ();
  g_key_file_load_from_file (desktop_keyfile, desktop_file, 0, &error);

  if (error) 
    {
      g_warning ("launcher-application.c: Unable to read from desktop file %s: %s", 
                desktop_file,
                error->message);
      g_error_free (error);
      return;
    }

  name = g_key_file_get_locale_string (desktop_keyfile,
                                      G_KEY_FILE_DESKTOP_GROUP,
                                      G_KEY_FILE_DESKTOP_KEY_NAME,
                                      NULL,
                                      &error);
  if (error) 
    {
      g_warning ("Unable to read from desktop file %s: %s", 
                desktop_file, 
                error->message);
      g_error_free (error);
      name = "";
    }
 

  icon_name = g_key_file_get_string (desktop_keyfile,
                                     G_KEY_FILE_DESKTOP_GROUP,
                                     G_KEY_FILE_DESKTOP_KEY_ICON,
                                     NULL);

  exec = g_key_file_get_string (desktop_keyfile,
                                G_KEY_FILE_DESKTOP_GROUP,
                                G_KEY_FILE_DESKTOP_KEY_EXEC,
                                NULL);
  
  comment = g_key_file_get_locale_string (desktop_keyfile,
                                          G_KEY_FILE_DESKTOP_GROUP,
                                          G_KEY_FILE_DESKTOP_KEY_COMMENT,
                                          NULL,
                                          &error);

  if (error) 
  {
    g_warning ("Unable to read comment from desktop file %s: %s", 
              desktop_file, 
              error->message);
    g_error_free (error);
    comment = "";
  }
  
  application->priv->name = name;
  application->priv->icon_name = icon_name;
  application->priv->exec = exec;
  application->priv->comment = comment;
  application->priv->desktop_file_path = desktop_file;


}


/**
 * launcher_application_get_running
 * @application: A #LauncherApplication
 * 
 * Method to tell if the @application is currently running or not
 * 
 * Returns: A Truth value
 */
gboolean
launcher_application_get_running (LauncherApplication *application) 
{
  g_return_val_if_fail (application, FALSE);
  
  return application->priv->running;
}

/**
 * launcher_application_get_running
 * @application: A #LauncherApplication
 * 
 * Method to tell if the @application is a favorite or not
 * 
 * Returns: A Truth value
 */
gboolean
launcher_application_get_favorite (LauncherApplication *application)
{
  g_return_val_if_fail (application, FALSE);
  
  return application->priv->favorite;
}

/**
 * launcher_application_get_categories
 * @application: A #LauncherApplication
 * 
 * provides the categories that @application is associated with
 * 
 * Returns: (transfer none): A GSList * of categories
 */
GSList *
launcher_application_get_categories (LauncherApplication *application)
{
  g_return_val_if_fail(application, NULL);
  
  return application->priv->categories;
}

/**
 * launcher_application_get_focused
 * @application: A #LauncherApplication
 *
 * provides a method that will return true if any of this @applicaiton's windows
 * are currently "focused"
 *
 * Returns: a gboolean value
 */
gboolean 
launcher_application_get_focused (LauncherApplication *application)
{
  g_return_val_if_fail (application, FALSE);
  
  return application->priv->focused;
}

/**
 * launcher_application_set_focused
 * @application: A #LauncherApplication
 * @window: A #WnckWindow
 * 
 * provides a mechanism to set @application as focused by providing it the 
 * focused @window (or null), this does not focus said @window - passing null
 * will not use the window tracking feature
 */

void
launcher_application_set_focused (LauncherApplication *application, 
                                  WnckWindow *window)
{
  g_return_if_fail (application);
  LauncherApplicationWindow *founditem;
  GTimeVal timeval;

  g_get_current_time (&timeval);
  
  if (window == NULL)
    {
      g_object_set (G_OBJECT (application),
                    "focused", TRUE,
                    NULL);
      return;
    }

   founditem = g_hash_table_lookup (application->priv->windows,
                                    window);

   if (founditem)
     {
       founditem->timestamp.tv_sec = timeval.tv_sec;
       founditem->timestamp.tv_usec = timeval.tv_usec;
     }
   else 
     {
       founditem = g_slice_new (LauncherApplicationWindow);
       founditem->window = window;
       founditem->timestamp.tv_sec = timeval.tv_sec;
       founditem->timestamp.tv_usec = timeval.tv_usec;
       g_hash_table_insert (application->priv->windows, window, founditem);
     }

   g_object_set (G_OBJECT (application),
                 "focused", TRUE,
                 NULL);
}

WnckWindow *
get_latest_window(LauncherApplication *app)
{
  // attempts to find the last focused wnckwindow
  WnckWindow *founditem = NULL;
  GTimeVal best_time;
  GHashTableIter iter;
  gpointer key, value;

  best_time.tv_sec = 0;
  best_time.tv_usec = 0;

  g_hash_table_iter_init (&iter, app->priv->windows);
  while (g_hash_table_iter_next (&iter, &key, &value))
    {
      LauncherApplicationWindow *appwin = (LauncherApplicationWindow *)value;
      if (WNCK_IS_WINDOW (appwin->window))
        {
          WnckWindowType type = wnck_window_get_window_type (appwin->window);
          
          if (type == WNCK_WINDOW_DESKTOP ||
              type == WNCK_WINDOW_DOCK ||
              type == WNCK_WINDOW_TOOLBAR ||
              type == WNCK_WINDOW_MENU ||
              type == WNCK_WINDOW_SPLASHSCREEN)
            {
              continue;
            }
          if (appwin->timestamp.tv_sec > best_time.tv_sec)
            {
              if (appwin->timestamp.tv_usec > best_time.tv_usec)
                {
                  best_time.tv_sec = appwin->timestamp.tv_sec;
                  best_time.tv_usec = appwin->timestamp.tv_usec;
                  founditem = appwin->window;
                }
            }
        }
      else 
        {
          // our window no longer exists, remove it from the table
          g_hash_table_remove (app->priv->windows, key);
        }
    }

  return founditem;
}


/**
 * launcher_application_show
 * @application: a #LauncherApplication
 *
 * this method will focus the latest un-minimized window this @application has
 * all this @application's windows are minimized then this method will 
 * unminimize them all
 */
void
launcher_application_show (LauncherApplication *application)
{
  g_return_if_fail (application);
  g_return_if_fail (application->priv->wnck_apps);
 
  /* FIXME
   * for now i am just grabbing the first wnckwindow we find thats not 
   * minimized, really we need to track what windows are focused so we know
   * the last focused window
   */
 
  GSList *a;
  GList *w;
  WnckWindow *found_window = NULL;

  // get the last focused window
  found_window = get_latest_window (application);

  if (found_window)
      /* just show this window */
      wnck_window_activate (found_window, gtk_get_current_event_time ());
  else
    {
      /* either there were no windows or all windows were minimized, so 
       * unminimize them
       */
      for (a = application->priv->wnck_apps; a; a = a->next)
        {
          WnckApplication *app = a->data;
          
          for (w = wnck_application_get_windows (app); w; w = w->next)
            {
              WnckWindow *window = w->data;
              wnck_window_activate (window, gtk_get_current_event_time ());
            }
        }
    }
}
  
