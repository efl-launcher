/*
 * Copyright (C) 2009 Canonical, Ltd.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 * version 3.0 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3.0 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Authored by Neil Jagdish Patel <neil.patel@canonical.com>
 */
/**
 * SECTION:launcher-menu
 * @short_description: Handles acquiring application menu data
 * @include: launcher-menu.h
 *
 * #LauncherMenu exists to provide an interface to the application menu and its 
 * children #LauncherCategory and #LauncherApplication objects
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gdk/gdk.h>
#include <gmenu-tree.h>

#include "launcher-menu.h"

#include "launcher-category.h"
#include "launcher-application.h"
#include "launcher-appman.h"

G_DEFINE_TYPE (LauncherMenu, launcher_menu, G_TYPE_OBJECT)

#define LAUNCHER_MENU_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE(obj, \
        LAUNCHER_TYPE_MENU, LauncherMenuPrivate))

struct _LauncherMenuPrivate
{
  GMenuTree *app_tree;
  GMenuTree *sys_tree;

  GSList *categories;
  GSList *applications;
  GSList *orphans;

  GSList *old_cats;

  guint    tag;
  gboolean refresh;
};

enum
{
  MENU_CHANGED,

  LAST_SIGNAL
};

static guint _menu_signals[LAST_SIGNAL] = { 0 };

static LauncherMenu *launcher_menu = NULL;

/* Forwards */
static GMenuTree * load_menu_from_tree (LauncherMenu    *menu,
                                        const gchar *menu_name);
static void        menu_changed        (GMenuTree   *tree,
                                        LauncherMenu    *menu);
static void        rehouse_orphans     (LauncherMenu *menu);

/* GObject Init */

static void
launcher_menu_finalize (GObject *menu)
{
  LauncherMenuPrivate *priv;
  GSList *l;

  g_return_if_fail (LAUNCHER_IS_MENU (menu));
  priv = LAUNCHER_MENU (menu)->priv;

  for (l = priv->categories; l; l = l->next)
    g_object_unref (l->data);

  g_slist_free (priv->categories);
  priv->categories = NULL;

  for (l = priv->applications; l; l = l->next)
    g_object_unref (l->data);

  g_slist_free (priv->applications);
  priv->applications = NULL;

  if (priv->app_tree)
    {
      gmenu_tree_item_unref (priv->app_tree);
      priv->app_tree = NULL;
    }
  if (priv->sys_tree)
    {
      gmenu_tree_item_unref (priv->sys_tree);
      priv->sys_tree = NULL;
    }

  G_OBJECT_CLASS (launcher_menu_parent_class)->finalize (menu);
}

static void
launcher_menu_class_init (LauncherMenuClass *klass)
{
  GObjectClass *obj_class = G_OBJECT_CLASS (klass);

  obj_class->finalize = launcher_menu_finalize;

  _menu_signals[MENU_CHANGED] =
    g_signal_new ("menu-changed",
                  G_OBJECT_CLASS_TYPE (obj_class),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (LauncherMenuClass, menu_changed),
                  NULL, NULL,
                  g_cclosure_marshal_VOID__VOID,
                  G_TYPE_NONE, 0);

  g_type_class_add_private (obj_class, sizeof (LauncherMenuPrivate));
}

static void
launcher_menu_init (LauncherMenu *menu)
{
  LauncherMenuPrivate *priv;

  priv = menu->priv = LAUNCHER_MENU_GET_PRIVATE (menu);

  priv->tag = 0;
  priv->refresh = FALSE;
  priv->categories = NULL;
  priv->applications = NULL;
  priv->old_cats = NULL;

  priv->app_tree = load_menu_from_tree (menu, "applications.menu");
  priv->sys_tree = load_menu_from_tree (menu, "settings.menu");

  rehouse_orphans (menu);
}

LauncherMenu *
launcher_menu_get_default (void)
{
  if (G_UNLIKELY (!LAUNCHER_IS_MENU (launcher_menu)))
    {
      launcher_menu = g_object_new (LAUNCHER_TYPE_MENU,
                                    NULL);
      return launcher_menu;
    }

  return g_object_ref (launcher_menu);
}

/* Private methods */

/*
 * Creates a LauncherCategory for the GMenuTreeDirectory, initialising it and
 * adding it to the list of categories.
 * If we are refreshing the menu, then before creating a new category, it
 * will search the existing categories, trying to find a match. This saves us
 * needlessly alloc/freeing lots of strings and structs.
 */
static LauncherCategory *
make_category (LauncherMenu *menu, GMenuTreeDirectory *directory)
{
  LauncherMenuPrivate *priv = menu->priv;
  LauncherCategory    *category = NULL;

  if (priv->refresh)
    {
      GSList       *c;
      const gchar  *name;
      const gchar  *comment;

      name = gmenu_tree_directory_get_name (directory);
      comment = gmenu_tree_directory_get_comment (directory);

      for (c = priv->old_cats; c; c = c->next)
        {
          LauncherCategory *old_cat = c->data;

          if (!old_cat)
            continue;

          if (g_strcmp0 (name, launcher_category_get_name (old_cat)) == 0
              && g_strcmp0 (comment, launcher_category_get_comment (old_cat)) ==0)
            {
              category = old_cat;
              break;
            }
        }
      if (category)
        {
          launcher_category_empty_applications (category);
          priv->categories = g_slist_append (priv->categories, category);
          priv->old_cats = g_slist_remove (priv->old_cats, category);

          return category;
        }
    }

  category = launcher_category_new (gmenu_tree_directory_get_name (directory),
                                    gmenu_tree_directory_get_comment (directory),
                                    gmenu_tree_directory_get_icon (directory));

  priv->categories = g_slist_append (priv->categories, category);

  return category;
}

/*
 * Fetches a LauncherApplication and adds it to the given category
 */
static LauncherApplication *
make_application (LauncherMenu       *menu,
                  GMenuTreeEntry *entry,
                  LauncherCategory   *category)
{
  LauncherMenuPrivate *priv = menu->priv;
  LauncherApplication *application = NULL;
  LauncherAppman      *appman = launcher_appman_get_default();
  application = launcher_appman_get_application_for_desktop_file 
                              (appman,
                               gmenu_tree_entry_get_desktop_file_path (entry));

  launcher_category_add_application (category, application);

  /* Add it to our list of applications */
  priv->applications = g_slist_append (priv->applications, application);


  return application;
}

static gint
sort_apps (LauncherApplication *a, LauncherApplication *b)
{
  return g_strcmp0 (launcher_application_get_name (a),
                    launcher_application_get_name (b));
}

/*
 * Recurses through the menu creating categories or application items in
 * memory. 'category' is the current category to add an application to
 */
static void
load_menu_from_directory (LauncherMenu           *menu,
                          GMenuTreeDirectory *dir,
                          LauncherCategory       *category)
{
  GSList *items, *i;

  if (dir == NULL)
    return;

  items = gmenu_tree_directory_get_contents (dir);
  for (i = items; i; i = i->next)
    {
      GMenuTreeItem *item = (GMenuTreeItem *)i->data;

      switch (gmenu_tree_item_get_type (item))
        {
        case GMENU_TREE_ITEM_DIRECTORY:
          if (category == NULL)
            {
              /* This category is in the root, so make a category for it */
              GMenuTreeDirectory *tree_directory;
              LauncherCategory *new_cat;

              tree_directory = GMENU_TREE_DIRECTORY (item);
              new_cat = make_category (menu, tree_directory);

              load_menu_from_directory (menu, tree_directory, new_cat);
            }
          else
            {
              /* After the re-spec of liblauncher LauncherCategory can only
               * contain LauncherApplications, thus this is harder to do
               * but obviously, not impossible. we have no need for it though
               */
              /* FIXME: At the moment, UNR requires a "flat" representation of
                 the menus i.e. only the top level categories are shown. We can
                 easily add support for the standard tree representation */
              load_menu_from_directory (menu, GMENU_TREE_DIRECTORY (item),
                                        category);
            }
          break;

        case GMENU_TREE_ITEM_ENTRY:
          if (category)
            make_application (menu, GMENU_TREE_ENTRY (item), category);
          else
            {
              menu->priv->orphans = g_slist_append (menu->priv->orphans,
                                                    gmenu_tree_item_ref (item));
              /*            
                */
            }
          break;

        default:
          break;
        }
      gmenu_tree_item_unref (item);
    }
  g_slist_free (items);
}

static void
rehouse_orphans (LauncherMenu *menu)
{
  LauncherMenuPrivate *priv = menu->priv;
  GSList *o;

  g_debug ("%s", G_STRLOC);

  for (o = priv->orphans; o; o = o->next)
    {
      GMenuTreeItem *item = o->data;    
      GSList *c;

      for (c = priv->categories; c; c = c->next)
        {
          LauncherCategory *cat = c->data;
          if (g_str_has_suffix (launcher_category_get_icon_name (cat),
                                "preferences-system"))
            {
              make_application (menu, GMENU_TREE_ENTRY (item), cat);
              launcher_category_sort_applications (cat,
                                                  (GCompareFunc)sort_apps);
            }
        }
      g_print ("ORPHAN: %s\n",
               gmenu_tree_entry_get_name (GMENU_TREE_ENTRY (item)));
      gmenu_tree_item_unref (item);
    }

  g_slist_free (priv->orphans);
  priv->orphans = NULL;
}

static GMenuTree *
load_menu_from_tree (LauncherMenu    *menu,
                     const gchar *menu_name)
{
  LauncherMenuPrivate    *priv;
  GMenuTreeDirectory *root;
  GMenuTree          *tree;

  g_return_val_if_fail (LAUNCHER_IS_MENU (menu), NULL);
  priv = menu->priv;

  /* Try and load the named menu */
  tree = gmenu_tree_lookup (menu_name, GMENU_TREE_FLAGS_NONE);
  if (!tree)
    {
      g_warning ("LauncherMenu: Unable to load '%s' menu.", menu_name);
      return NULL;
    }

  /* Grab the root directory of the menu, and then load it into memory */
  root = gmenu_tree_get_root_directory (tree);
  load_menu_from_directory (menu, root, NULL);
  gmenu_tree_item_unref (root);

  /* Add a monitor to react to menu changes */
  gmenu_tree_add_monitor (tree, (GMenuTreeChangedFunc)menu_changed, menu);

  return tree;
}

static void
free_old_menu (LauncherMenu *menu)
{
  LauncherMenuPrivate *priv = menu->priv;
  GSList *l;

  for (l = priv->old_cats; l; l = l->next)
    g_object_unref(l->data);

  g_slist_free (priv->old_cats);
  priv->old_cats = NULL;

}

static gboolean
reload_menu (LauncherMenu *menu)
{
  LauncherMenuPrivate    *priv;
  GMenuTreeDirectory *root;

  g_return_val_if_fail (LAUNCHER_IS_MENU (menu), FALSE);
  priv = menu->priv;

  /* Move current categorys and applications lists to 'old' lists. These
   * lists are searched by make_category and make_application for already-
   * loaded items. If a category or application was already loaded, it will be
   * removed from the old_ list and added to the correct, current, list. At the
   * end of the reload, the old_ lists, and there contents, are freed.
   */
  priv->old_cats = priv->categories;
  priv->categories = NULL;

  /* Let make_category and make_application know we're refreshing the list */
  priv->refresh = TRUE;

  root = gmenu_tree_get_root_directory (priv->app_tree);
  load_menu_from_directory (menu, root, NULL);
  gmenu_tree_item_unref (root);

  root = gmenu_tree_get_root_directory (priv->sys_tree);
  load_menu_from_directory (menu, root, NULL);
  gmenu_tree_item_unref (root);

  rehouse_orphans (menu);

  priv->refresh = FALSE;

  free_old_menu (menu);

  g_debug ("LauncherMenu: Desktop menu changed");

  g_signal_emit (menu, _menu_signals[MENU_CHANGED], 0);

  priv->tag = 0;
  return FALSE;
}

static void
menu_changed (GMenuTree *tree, LauncherMenu *menu)
{
  LauncherMenuPrivate *priv;

  g_return_if_fail (LAUNCHER_IS_MENU (menu));
  priv = menu->priv;

  if (priv->tag)
    return;

  /* When the menu changes, we may get 5-10 of these notifications, so we add
     a timeout to grab them all, and then reload the menu */
  priv->tag = g_timeout_add (200, (GSourceFunc)reload_menu, menu);
}

/*
 * Public Methods
 */
/**
 * launcher_menu_get_categories:
 * @menu: a #LauncherMenu object
 *
 * This will return a list of child categories in the given @menu
 *
 * Returns: #GSList of #LauncherCategory or NULL
 */
GSList *
launcher_menu_get_categories (LauncherMenu *menu)
{
  g_return_val_if_fail (LAUNCHER_IS_MENU (menu), NULL);
  return menu->priv->categories;
}


/**
 * launcher_menu_get_applications:
 * @menu: a #LauncherMenu object
 *
 * This will return a list of child applications in the given @menu
 *
 * Returns: #GSList of #LauncherApplication or NULL
 */
GSList *
launcher_menu_get_applications (LauncherMenu *menu)
{
  g_return_val_if_fail (LAUNCHER_IS_MENU (menu), NULL);
  return menu->priv->applications;
}
