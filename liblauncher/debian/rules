#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

# These are used for cross-compiling and for saving the configure script
# from having to guess our platform (since we know it already)
DEB_HOST_GNU_TYPE   ?= $(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)
DEB_BUILD_GNU_TYPE  ?= $(shell dpkg-architecture -qDEB_BUILD_GNU_TYPE)
ifneq ($(DEB_HOST_GNU_TYPE),$(DEB_BUILD_GNU_TYPE))
CROSS= --build $(DEB_BUILD_GNU_TYPE) --host $(DEB_HOST_GNU_TYPE)
else
CROSS= --build $(DEB_BUILD_GNU_TYPE)
endif

config: config-stamp
config-stamp: 
	dh_testdir
	[ ! -d debian/patches ] || $(MAKE) -f /usr/share/quilt/quilt.make patch
ifneq "$(wildcard /usr/share/misc/config.sub)" ""
	cp -f /usr/share/misc/config.sub config.sub
endif
ifneq "$(wildcard /usr/share/misc/config.guess)" ""
	cp -f /usr/share/misc/config.guess config.guess
endif
	autoreconf --force
	./configure $(CROSS) \
		--prefix=/usr \
		--datarootdir=\$${prefix}/share \
		--sysconfdir=/etc \
		--mandir=\$${prefix}/share/man \
		--infodir=\$${prefix}/share/info \
		--disable-silent-rules \
		CFLAGS="$(CFLAGS)" \
		LDFLAGS="-Wl,-z,defs -Wl,--as-needed -Wl,--no-undefined" \
		PACKAGE="efl-launcher" \
		GETTEXT_PACKAGE="efl-launcher" \
		GCONF_SCHEMA_FILE_DIR=/usr/share/gconf/schemas 
	touch $@

build: build-stamp
build-stamp: config
	dh_testdir
	$(MAKE) V=1
	touch $@

clean:
	dh_testdir
	dh_testroot
	rm -f build-stamp config-stamp
	[ ! -f Makefile ] || $(MAKE) distclean
	rm -f config.sub config.guess config.cache config.log
	rm -fv configure aclocal.m4 `find . -name "Makefile.in"` `find . -name "Makefile"` po/POTFILES

	[ ! -d debian/patches ] || $(MAKE) -f /usr/share/quilt/quilt.make unpatch
	dh_clean

install: build
	dh_testdir
	dh_testroot
	dh_prep
	dh_installdirs
	$(MAKE) V=1 DESTDIR=$(CURDIR)/debian/tmp install
	find debian/tmp/usr/lib -name \*.la -exec rm {} \;
	find debian/tmp/usr/lib -name \*.a -exec rm {} \;

# Build architecture-independent files here.
binary-indep: install

# Build architecture-dependent files here.
binary-arch: install
	dh_testdir
	dh_testroot
	dh_installchangelogs -a
	dh_installdocs -a
	dh_installexamples -a
	dh_install -a
#	dh_installmenu -a
#	dh_installmime -a
	dh_installman -a
	dh_link -a
	dh_strip -a --dbg-package=liblauncher-0.3-dbg
	dh_compress -a
	dh_fixperms -a
	dh_makeshlibs -a -V 'liblauncher-0.3-0 (>= 0.3.6)'
	[ ! -e /usr/bin/dh_buildinfo ] || dh_buildinfo -a
	dh_installdeb -a
	dh_shlibdeps -a
	dh_gencontrol -a
	dh_md5sums -a
	dh_builddeb -a

binary: binary-indep binary-arch
.PHONY: config build clean binary-indep binary-arch binary install 
