/*
 * Copyright (C) 2009 Canonical Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as 
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authored by Neil Jagdish Patel <neil.patel@canonical.com>
 *
 */

#include <glib.h>
#include <launcher/launcher.h>

void test_icon_utils_create_suite (void);
void test_launcher_create_suite (void);

void test_category_create_suite (void);

gint
main (gint argc, gchar *argv[])
{
  g_type_init (); 
  g_test_init (&argc, &argv, NULL);

  gtk_init (&argc, &argv);

  /* Please keep this alphabetically ordered */
  test_icon_utils_create_suite ();
  test_launcher_create_suite ();
  test_category_create_suite ();

  return g_test_run ();
}
