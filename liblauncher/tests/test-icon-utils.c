/*
 * Copyright (C) 2009 Canonical Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as 
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authored by Neil Jagdish Patel <neil.patel@canonical.com>
 *
 */

#include <glib.h>
#include <launcher/launcher.h>

static gchar *bad_uris[] = 
{
  "file:///jewijfoewjfoiwejfoiwjefiwfjwiofwjfwijf/kwefwfej/jfeowjefowif",
  "file:/\\/",
  "sftp://thislocationdoesntexist/well/hopefullynot",
  NULL
};

void
test_valid_uri (void)
{
  gchar *cwd;
  //gchar *filename;
  gchar *uri;
  gchar *icon_name;

  cwd = g_get_current_dir ();

  /* Test the tests folder */
  {
    uri = g_filename_to_uri (cwd, NULL, NULL);
    icon_name = launcher_icon_utils_icon_name_for_volume_uri (uri);

    g_assert (icon_name);
    g_free (icon_name);
    g_free (uri);
  }

  g_free (cwd);
}

/*
 * Any bad uri's should yeild the default "folder" icon name
 */
void
test_invalid_uri (void)
{
  gint i = 0; 

  for (i=0; i < G_N_ELEMENTS (bad_uris); i++)
    {
      gchar *icon_name;

      icon_name = launcher_icon_utils_icon_name_for_volume_uri (bad_uris[i]);

      g_assert_cmpstr (icon_name, ==, "folder");

      g_free (icon_name);
    }
}

void
test_icon_utils_create_suite (void)
{
#define DOMAIN "/IconUtils"
  
  g_test_add_func (DOMAIN"/IconNameForVolumeUri/ValidUri", test_valid_uri);
  g_test_add_func (DOMAIN"/IconNameForVolumeUri/InvalidUri", test_invalid_uri);
}
