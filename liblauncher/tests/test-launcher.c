/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * Copyright (C) 2009 Canonical Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as 
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authored by David Barth <david.barth@canonical.com>
 *
 */
#include <stdlib.h>
#include <glib.h>
#include <launcher/launcher.h>

// #include "test-set.h"
#define TEST_DESKTOP_FILE TESTDIR"/firefox.desktop"
#define TEST_DESKTOP_FILE_BROKEN TESTDIR"/firefox-broken-URL.desktop"

void
test_appman_get_default (void)
{
	LauncherAppman *appman = launcher_appman_get_default ();
	g_assert (appman != NULL);
}

void
test_get_application_for_desktop_file (void)
{
	LauncherAppman *appman = launcher_appman_get_default ();

	LauncherApplication *app = launcher_appman_get_application_for_desktop_file (appman, TEST_DESKTOP_FILE);
	g_assert (app != NULL);

  if (g_test_trap_fork (0, G_TEST_TRAP_SILENCE_STDOUT |
                         G_TEST_TRAP_SILENCE_STDERR))
    {
      LauncherApplication *app2 = launcher_appman_get_application_for_desktop_file (appman, TEST_DESKTOP_FILE_BROKEN);
      g_assert (app2 != NULL);
      exit (0);
    }
  g_test_trap_assert_failed ();
}

void
test_launcher_create_suite (void)
{
#define DOMAIN "/Launcher"
  
  g_test_add_func (DOMAIN"/Appman/GetDefault", test_appman_get_default);
  g_test_add_func (DOMAIN"/Appman/GetApplicationForDesktopFile", 
                   test_get_application_for_desktop_file);
}
