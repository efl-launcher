/*
 * Copyright (C) 2009 Canonical Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as 
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authored by Gordon Allott <gord.allott@canonical.com>
 *
 */

#include <glib.h>
#include <launcher/launcher.h>

#define TEST_DESKTOP_FILE TESTDIR"/firefox.desktop"

void
test_create_new_category (void)
{
  LauncherCategory *category = launcher_category_new ("foobar", "Hello World!", NULL);
  g_assert (category != NULL);
}

void
test_applications (void)
{
  GSList *applist = NULL;
  
  LauncherAppman *appman = launcher_appman_get_default ();
  LauncherCategory *category = launcher_category_new ("foobar", "hello world", NULL);
  LauncherApplication *app = launcher_appman_get_application_for_desktop_file (appman, TEST_DESKTOP_FILE);
 
  launcher_category_add_application (category, app);
  applist = launcher_category_get_applications (category);
  
  g_assert (applist != NULL);
  g_assert (applist->data == app);
  
  launcher_category_remove_application (category, app);
  applist = launcher_category_get_applications (category);
  
  g_assert (applist == NULL);
}

void
test_category_create_suite (void)
{
#define DOMAIN "/Launcher"
  
  g_test_add_func (DOMAIN"/Category", test_create_new_category);
  g_test_add_func (DOMAIN"/Category/Applications", test_applications);
}
