/*
 * This file is part of Netbook Launcher EFL
 * Copyright (C) 2007-2009 Instituto Nokia de Tecnologia
 * Contact: Renato Chencarek <renato.chencarek@openbossa.org>
 *          Eduardo Lima (Etrunko) <eduardo.lima@openbossa.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#define RECALC_FROM_MOUSE()                                             \
   script {                                                             \
      public is_mouse_down = 0;                                         \
      public enabled = 1;                                               \
                                                                        \
      public recalc_from_mouse(pos, part_name, knob) {                  \
         new mx, my, ox, oy, ow, oh, res;                               \
         new Float:p;                                                   \
                                                                        \
         get_mouse(mx, my);                                             \
         get_geometry(part_name, ox, oy, ow, oh);                       \
                                                                        \
         if (pos == 0) {                                                \
            res = mx - ox;                                              \
            if (res > 0) {                                              \
               p = Float:res / Float:ow;                                \
               set_drag(knob, p, 0.0);                                  \
            }                                                           \
            else                                                        \
               set_drag(knob, 0.0, 0.0);                                \
         }                                                              \
         else {                                                         \
            res = my - oy;                                              \
            if (res > 0) {                                              \
               p = Float:res / Float:oh;                                \
               set_drag(knob, 0.0, p);                                  \
            }                                                           \
            else                                                        \
               set_drag(knob, 0.0, 0.0);                                \
         }                                                              \
      }                                                                 \
                                                                        \
      public send_drag_signal(signal_name[], knob) {                    \
         new Float:dx;                                                  \
         new Float:dy;                                                  \
         new buff[256];                                                 \
         get_drag(knob, dx, dy);                                        \
         snprintf(buff, 256, "%f:%f", dx, dy);                          \
         emit("drag", "elm.dragable.vbar");                      \
      }                                                                 \
   }                                                                    \

#define SLIDER_PROGRAMS(part_name, knob, pos)                           \
   RECALC_FROM_MOUSE();                                                 \
                                                                        \
   parts {                                                              \
   part {                                                               \
      name: part_name"_area";                                           \
      type: RECT;                                                       \
      mouse_events: 1;                                                  \
      description {                                                     \
         state: "default" 0.0;                                          \
         color: 255 0 0 0;                                              \
         rel1.to: part_name;                                            \
         rel2.to: part_name;                                            \
      }                                                                 \
   }                                                                    \
   }                                                                    \
                                                                        \
   programs {                                                           \
      program {                                                         \
         name: part_name"_show";                                        \
         signal: "show";                                                \
         source: "*";                                                   \
         script {                                                       \
            set_int(is_mouse_down, 0);                                  \
            set_int(enabled, 1);                                        \
         }                                                              \
      }                                                                 \
                                                                        \
      program {                                                         \
         name: part_name"_clicked";                                     \
         signal: "mouse,clicked,1";                                     \
         source: part_name"_area";                                      \
         script {                                                       \
            if (get_int(enabled) == 1)                                  \
               recalc_from_mouse(pos, PART:part_name"_area", PART:knob); \
         }                                                              \
      }                                                                 \
                                                                        \
      program {                                                         \
         name: part_name"_down";                                        \
         signal: "mouse,down,1";                                        \
         source: part_name"_area";                                      \
         script {                                                       \
            if (get_int(enabled) == 1)                                  \
               set_int(is_mouse_down, 1);                               \
         }                                                              \
      }                                                                 \
                                                                        \
      program {                                                         \
         name: part_name"_up";                                          \
         signal: "mouse,up,1";                                          \
         source: part_name"_area";                                      \
         script {                                                       \
            if (get_int(enabled) == 1) {                                \
               set_int(is_mouse_down, 0);                               \
               recalc_from_mouse(pos, PART:part_name"_area", PART:knob); \
               send_drag_signal(part_name",drag_set", PART:knob);       \
            }                                                           \
         }                                                              \
      }                                                                 \
                                                                        \
      program {                                                         \
         name: part_name"_move";                                        \
         signal: "mouse,move";                                          \
         source: part_name"_area";                                      \
         script {                                                       \
            if (get_int(enabled) == 1 && get_int(is_mouse_down) == 1) { \
               recalc_from_mouse(pos, PART:part_name"_area", PART:knob); \
               send_drag_signal(part_name",current_pos", PART:knob);    \
            }                                                           \
         }                                                              \
      }                                                                 \
                                                                        \
      program {                                                         \
         name: knob"_drag_stop";                                        \
         signal: "drag,stop";                                           \
         source: "knob";                                                \
         script {                                                       \
            if (get_int(enabled) == 1)                                  \
               send_drag_signal(part_name",drag_set", PART:knob);       \
         }                                                              \
      }                                                                 \
                                                                        \
      program {                                                         \
         name: knob"_disable";                                          \
         signal: "drag,disable";                                        \
         source: part_name;                                             \
         script {                                                       \
            set_int(enabled, 0);                                        \
         }                                                              \
      }                                                                 \
                                                                        \
      program {                                                         \
         name: knob"_enable";                                           \
         signal: "drag,enable";                                         \
         source: part_name;                                             \
         script {                                                       \
            set_int(enabled, 1);                                        \
         }                                                              \
      }                                                                 \
   }
