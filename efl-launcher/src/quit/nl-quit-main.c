/* -*- Mode: C; indent-tabs-mode: nil; tab-width: 2 -*-
 *
 * Copyright (C) 2009 Canonical Ltd.
 * Authors:
 *  Gustavo Sverzut Barbieri <gustavo.barbieri@canonical.com>
 *
 * This file is part of Netbook Launcher EFL.
 *
 * Netbook Launcher EFL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * Netbook Launcher EFL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Netbook Launcher EFL.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include "nl-quit.h"
#include <locale.h>
#include <libgnomeui/libgnomeui.h>
#include <libgnome/libgnome.h>

int
main(int argc, char *argv[])
{
  GtkWidget *window;
  gint result;

  setlocale(LC_ALL, "");
  bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR);
  bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
  textdomain(GETTEXT_PACKAGE);

  gnome_program_init("netbook-launcher-efl_quit", "0.1",
                     LIBGNOMEUI_MODULE, argc, argv,
                     GNOME_PARAM_NONE, NULL);

  window = nl_quit_new();
  result = gtk_dialog_run (GTK_DIALOG (window));
  while (gtk_events_pending())
      gtk_main_iteration_do(TRUE);
  g_debug ("reply: %d", result);

  return 0;
}
