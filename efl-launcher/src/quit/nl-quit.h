/* -*- Mode: CHeader; indent-tabs-mode: nil; tab-width: 2 -*-
 *
 * Copyright (C) 2008 Canonical Ltd.
 * Authors:
 *  Neil Jagdish Patel <neil.patel@canonical.com>
 *
 * This file is part of Netbook Launcher EFL.
 *
 * Netbook Launcher EFL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * Netbook Launcher EFL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Netbook Launcher EFL.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _NL_QUIT_H_
#define _NL_QUIT_H_

#include <glib-object.h>
#include <gtk/gtk.h>

GtkWidget * nl_quit_new      ();


#endif /* _NL_QUIT_H_ */
