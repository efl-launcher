/* -*- Mode: C; indent-tabs-mode: nil; tab-width: 2 -*-
 *
 * Copyright (C) 2009 Canonical Ltd.
 * Authors:
 *  Gustavo Sverzut Barbieri <gustavo.barbieri@canonical.com>
 *
 * This file is part of Netbook Launcher EFL.
 *
 * Netbook Launcher EFL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * Netbook Launcher EFL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Netbook Launcher EFL.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "netbook-launcher.h"
#include <Elementary.h>
#include <stdio.h>

struct error_ctxt
{
  Evas_Object *inwin;
  Evas_Object *label;
};

static struct error_ctxt ctxt = {NULL, NULL};

static void
_error_dismiss(void *data, Evas_Object *o, void *event_info __UNUSED__)
{
  struct error_ctxt *ctxt = data;
  evas_object_hide(ctxt->inwin);
  sound_play(o, "button-pressed");
  sound_play(o, "button-released");
  sound_play(ctxt->inwin, "dialog-ok");
}

static void
_error_del(void *data, Evas *e __UNUSED__, Evas_Object *o __UNUSED__, void *event_info __UNUSED__)
{
  struct error_ctxt *ctxt = data;
  ctxt->inwin = NULL;
  ctxt->label = NULL;
}

Evas_Object *
error_add(Evas_Object *parent)
{
  Evas_Object *inwin, *frame, *box, *button, *label, *icon;

  if (ctxt.inwin)
    return ctxt.inwin;

  inwin = elm_win_inwin_add(parent);
  if (!inwin)
  {
    ERR("could not create error reporting system.\n");
    return NULL;
  }
  elm_object_style_set(inwin, "error");
  evas_object_event_callback_add(inwin,  EVAS_CALLBACK_FREE, _error_del, &ctxt);

  frame = elm_frame_add(inwin);
  if (!frame)
  {
    ERR("could not create error frame widget.\n");
    evas_object_del(inwin);
    return NULL;
  }
  elm_object_style_set(frame, "error");

  box = elm_box_add(inwin);
  if (!box)
  {
    ERR("could not create error box widget.\n");
    evas_object_del(inwin);
    return NULL;
  }

  button = elm_button_add(box);
  if (!button)
  {
    ERR("could not create error button widget.\n");
    evas_object_del(inwin);
    return NULL;
  }
  evas_object_smart_callback_add(button, "clicked", _error_dismiss, &ctxt);

  label = elm_label_add(box);
  if (!label)
  {
    ERR("could not create error label widget.\n");
    evas_object_del(inwin);
    return NULL;
  }

  icon = elm_icon_add(button);
  if (icon)
  {
    elm_icon_standard_set(icon, "close");
    elm_icon_scale_set(icon, 1, 0);
    evas_object_show(icon);
    elm_button_icon_set(button, icon);
  }
  elm_button_label_set(button, "Dismiss");

  elm_box_pack_start(box, label);
  elm_box_pack_end(box, button);

  elm_frame_label_set(frame, "Error");
  elm_frame_content_set(frame, box);

  elm_win_inwin_content_set(inwin, frame);

  evas_object_show(label);
  evas_object_show(button);
  evas_object_show(box);
  evas_object_show(frame);

  ctxt.inwin = inwin;
  ctxt.label = label;

  return inwin;
}

void
error_show(const char *fmt, ...)
{
   char *str;
   va_list args;

   va_start(args, fmt);
   if (!ctxt.label)
     {
	eina_log_vprint
	  (EINA_LOG_DOMAIN_GLOBAL, EINA_LOG_LEVEL_ERR, __FILE__, __FUNCTION__,
       __LINE__, fmt, args);
	return;
     }

   if (vasprintf(&str, fmt, args) == -1)
     {
	eina_log_vprint
	  (EINA_LOG_DOMAIN_GLOBAL, EINA_LOG_LEVEL_ERR, __FILE__, __FUNCTION__,
       __LINE__, fmt, args);
	return;
     }

   elm_label_label_set(ctxt.label, str);
   free(str);
   elm_win_inwin_activate(ctxt.inwin);
   sound_play(ctxt.inwin, "dialog-error");
}
