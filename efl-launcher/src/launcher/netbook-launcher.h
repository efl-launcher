/* -*- Mode: CHeader; indent-tabs-mode: nil; tab-width: 2 -*-
 *
 * Copyright (C) 2009,2010 Canonical Ltd.
 * Authors:
 *  Gustavo Sverzut Barbieri <gustavo.barbieri@canonical.com>
 *  Michael Terry <michael.terry@canonical.com>
 *
 * This file is part of Netbook Launcher EFL.
 *
 * Netbook Launcher EFL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * Netbook Launcher EFL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Netbook Launcher EFL.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __NETBOOK_LAUNCHER_H__
#define __NETBOOK_LAUNCHER_H__ 1

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <Evas.h>
#include <Ecore.h>
#include <gio/gio.h>
#include <launcher/launcher.h>

#ifdef HAVE_GOOGLE_GADGETS
#include <cairo.h>
#endif

#define DBG(...) EINA_LOG_DBG(__VA_ARGS__)
#define INF(...) EINA_LOG_INFO(__VA_ARGS__)
#define WRN(...) EINA_LOG_WARN(__VA_ARGS__)
#define ERR(...) EINA_LOG_ERR(__VA_ARGS__)

/* comment to disable collapsing all preference items into single entry */
#define PREFS_COLLAPSE
#define PREFS_MATCH_STR "preferences"

/* main */

struct prefs
{
  const char *theme;
};

struct item_size
{
  Evas_Coord w, h;
  unsigned int icon_size;
};

extern struct prefs _nl_prefs;

Evas_Object *_nl_icon_load_sized(Evas_Object *parent, const char *path, unsigned int size);
void         _nl_edje_part_size_get(Evas_Object *parent, const char *group, const char *part, Evas_Coord *w, Evas_Coord *h);
Eina_Bool    _nl_edje_item_size_calc(struct item_size *is, Evas_Object *parent, const char *group);
void         _nl_restart(void);

Eina_Bool    _nl_icon_info_event_enter(const char *name, const char *icon, const char *comment);
Eina_Bool    _nl_icon_info_event_leave(void);
Eina_Bool    _nl_theme_resolve(char *buf, const char *value);

extern int EVENT_ICON_INFO_ENTER;
extern int EVENT_ICON_INFO_LEAVE;

struct icon_info_event_enter
{
  const char *name;
  const char *icon;
  const char *comment;
};

/* win */
Evas_Object *win_add(void);
Eina_Bool    win_sidebar_set(Evas_Object *win, Evas_Object *categories);
Eina_Bool    win_apps_set(Evas_Object *win, Evas_Object *apps);
Eina_Bool    win_apps_show_category(Evas_Object *win, const char *category);
Eina_Bool    win_apps_show_favorites(Evas_Object *win);
Eina_Bool    win_apps_show_gadgets(Evas_Object *win, const char *category);
Eina_Bool    win_places_set(Evas_Object *win, Evas_Object *places);
Eina_Bool    win_places_show(Evas_Object *win);
Eina_Bool    win_preferences_set(Evas_Object *win, Evas_Object *prefs);
Eina_Bool    win_preferences_show(Evas_Object *win);
Eina_Bool    win_bg_set(Evas_Object *win, Evas_Object *bg);
Eina_Bool    win_quit_show(Evas_Object *win);
Evas_Object *win_sidebar_get(const Evas_Object *win);

struct string_array
{
  unsigned int len;
  const char **array; /* Is NULL terminated. May be NULL if len == 0 */
};

extern int WIN_EVENT_FOCUS_IN;
extern int WIN_EVENT_FOCUS_OUT;
extern int WIN_EVENT_APP_OPEN;
extern int WIN_EVENT_APP_CLOSE;
extern int WIN_EVENT_APPS_CHANGED;

/* sidebar */
Evas_Object *sidebar_add(Evas_Object *parent);
#define CALLBACK_SIDEBAR_MENU_SELECTED "sidebar-menu-selected"
#define CALLBACK_SIDEBAR_FAVORITES_SELECTED "sidebar-favorites-selected"
#define CALLBACK_SIDEBAR_GADGETS_SELECTED "sidebar-gadgets-selected"
#define CALLBACK_SIDEBAR_PLACES_SELECTED "sidebar-places-selected"
#define CALLBACK_SIDEBAR_QUIT_SELECTED "sidebar-quit-selected"
#define CALLBACK_SIDEBAR_PREFERENCES_SELECTED "sidebar-preferences-selected"
void sidebar_favorites_activate(Evas_Object *sidebar);

/* apps */

extern int FAVORITES_EVENT_CHANGED;
#define CALLBACK_APPS_QUIT_SELECTED "apps-quit-selected"

Evas_Object *apps_add(Evas_Object *parent);
Eina_Bool    apps_load_category(Evas_Object *apps, const char *category, Eina_Bool animate, void (*end_callback)(void *data, Evas_Object *apps), const void *data);
Eina_Bool    apps_load_favorites(Evas_Object *apps, Eina_Bool animate, void (*end_callback)(void *data, Evas_Object *apps), const void *data);
Eina_Bool    apps_load_gadgets(Evas_Object *apps, Eina_Bool animate, void (*end_callback)(void *data, Evas_Object *apps), const void *end_callback_data);

/* app_item */
struct app_item_common
{
  Evas_Object *parent;
  Evas_Object *scroller;
  Evas_Object *container;
  const struct string_array *running_apps; /* received from win */
  struct item_size item;
  LauncherMenu *menu; /* existing launcher menu reference */
  LauncherFavorites *favs; /* existing launcher favorites reference */
  struct kbdnav_pool *nav_pool;

  Ecore_Event_Handler *_apps_changed; /* internal */
};

Eina_Bool app_item_common_ecore_start(struct app_item_common *common, Evas_Object *parent, Evas_Object *scroller, Evas_Object *container);
Eina_Bool app_item_common_ecore_stop(struct app_item_common *common);
Eina_Bool app_item_common_navigation_id_set(struct app_item_common *common, const char *id);

Eina_Bool app_item_common_glib_start(struct app_item_common *common);
Eina_Bool app_item_common_glib_stop(struct app_item_common *common);

Evas_Object *app_item_add(const struct app_item_common *parent, const char *category, const char *name, const char *comment, const char *icon_name, const char *match_name, const char *uid, Eina_Bool is_favorite);

#ifdef HAVE_GOOGLE_GADGETS
Evas_Object *app_gadget_add(const struct app_item_common *parent, const char *category, const char *name, const char *comment, const char *icon_name, const char *path, const char *uid, Eina_Bool is_favorite);
#endif

/* places */
struct places_item_common
{
  const char *id;
  Evas_Object *obj;
  void (*select)(struct places_item_common *common);
  void (*unselect)(struct places_item_common *common);
  void (*key_down)(struct places_item_common *common, Evas_Event_Key_Down *ev);
};

Evas_Object *places_add(Evas_Object *parent);
Eina_Bool    places_load(Evas_Object *places);
Evas_Object *places_section_add(Evas_Object *places, const char *label);
Eina_Bool    places_navigation_item_add(Evas_Object *places, struct places_item_common *item);
Eina_Bool    places_navigation_item_del(Evas_Object *places, struct places_item_common *item);

void         places_item_common_select(Evas_Object *places, struct places_item_common *common);
void         places_item_common_unselect(Evas_Object *places, struct places_item_common *common);

/* places_folders */
Evas_Object *places_folders_add(Evas_Object *parent);

/* places_storage */
Evas_Object *places_storage_add(Evas_Object *parent);

/* places_recent */
Evas_Object *places_recent_add(Evas_Object *parent);

/* preferences */
Evas_Object *preferences_add(Evas_Object *parent);
Eina_Bool    preferences_load(Evas_Object *preferences);

/* error */
Evas_Object *error_add(Evas_Object *parent);
void         error_show(const char *fmt, ...);

/* gstuff */

struct func_ctxt
{
  void (*func)(void *data);
  void (*free)(void *data);
  void *data;
};

Eina_Bool    gstuff_init(void);
void         gstuff_shutdown(void);

Eina_Bool    gstuff_glib_run(struct func_ctxt *ctxt);
Eina_Bool    gstuff_ecore_run(struct func_ctxt *ctxt);

Eina_Bool    gstuff_glib_exit(void);
Eina_Bool    gstuff_ecore_exit(void);

Eina_Bool    gstuff_glib_run_noargs(void (*func)(void));
Eina_Bool    gstuff_ecore_run_noargs(void (*func)(void));

Eina_Bool    gstuff_glib_run_simple(void (*func)(void *data), const void *data);
Eina_Bool    gstuff_ecore_run_simple(void (*func)(void *data), const void *data);

Eina_Bool    gstuff_glib_run_string(void (*func)(const char *str), const char *str);
Eina_Bool    gstuff_ecore_run_string(void (*func)(const char *str), const char *str);

Eina_Bool    gstuff_glib_run_ptr_string(void (*func)(void *data, const char *str), const void *data, const char *str);
Eina_Bool    gstuff_ecore_run_ptr_string(void (*func)(void *data, const char *str), const void *data, const char *str);


Eina_Bool    gstuff_glib_run_string_array(void (*func)(unsigned int count, const char **array), unsigned int count, const char **array);
Eina_Bool    gstuff_ecore_run_string_array(void (*func)(unsigned int count, const char **array), unsigned int count, const char **array);


Eina_Bool    gstuff_glib_run_ptr_string_array(void (*func)(void *data, unsigned int count, const char **array), const void *data, unsigned int count, const char **array);
Eina_Bool    gstuff_ecore_run_ptr_string_array(void (*func)(void *data, unsigned int count, const char **array), const void *data, unsigned int count, const char **array);

char *gstuff_pixbuf_to_file(GdkPixbuf *pixbuf, const char *name);

guint gstuff_keyname_to_key(const char *keyname);

/* bg */
Evas_Object *bg_add(Evas_Object *parent);

/* launchfeedback */
Evas_Object *launchfeedback_add(Evas_Object *parent);
void         launchfeedback_show_full(const char *icon_path, const char *label, double timeout, void (*hide_cb)(void *data), const void *hide_data);
void         launchfeedback_show(const char *icon_path, const char *label, double timeout);
void         launchfeedback_hide(void);

/* icon (NOTE: just use from glib thread, see gstuff) */
Eina_Bool    icon_path_init(void);
void         icon_path_shutdown(void);
const char  *icon_path_get(const char *name, unsigned int size);
const char  *icon_gicon_path_get(GIcon *icon, unsigned int size);
const char  *icon_mime_type_path_get(const char *mime_type, unsigned int size);
const char  *icon_unr_path_get(const char *icon_name, unsigned int icon_size);

/* icon_async (NOTE: just use from ecore) */
Evas_Object *icon_async_add(Evas_Object *parent);
Eina_Bool    icon_async_size_set(Evas_Object *icon, Evas_Coord w, Evas_Coord h);
Eina_Bool    icon_async_file_set(Evas_Object *icon, const char *file);

/* conf */
unsigned int conf_init(void);
unsigned int conf_shutdown(void);
Eina_Bool    conf_bg_set(const char *filename, unsigned int option, unsigned int color, Eina_Bool draw);
Eina_Bool    conf_bg_get(const char **filename, unsigned int *option, unsigned int *color, Eina_Bool *draw);
Eina_Bool    conf_font_set(const char *section, const char *font, unsigned int size);
Eina_Bool    conf_font_get(const char *section, const char **font, unsigned int *size);
Eina_Bool    conf_dpi_set(double dpi);
Eina_Bool    conf_dpi_get(double *dpi);
Eina_Bool    conf_theme_set(const char *theme_path);
Eina_Bool    conf_theme_get(const char **theme_path);

/* kbdnav: all strings are stringshared */
struct kbdnav_pool *kbdnav_pool_new(Evas_Object *(*object_get)(void *user_data, void *item_data), const char *(*id_get)(void *user_data, void *item_data), void (*select)(void *user_data, void *item_data), void (*unselect)(void *user_data, void *item_data), const void *user_data);
void                kbdnav_pool_free(struct kbdnav_pool *pool);

Eina_Bool           kbdnav_pool_item_add(struct kbdnav_pool *pool, const void *data);
Eina_Bool           kbdnav_pool_item_del(struct kbdnav_pool *pool, const void *data);
Eina_Bool           kbdnav_pool_current_id_set(struct kbdnav_pool *pool, const char *id);
void               *kbdnav_pool_current_item_get(struct kbdnav_pool *pool);
Evas_Object        *kbdnav_pool_current_object_get(struct kbdnav_pool *pool);
Eina_Bool           kbdnav_pool_current_select(struct kbdnav_pool *pool);
Eina_Bool           kbdnav_pool_current_unselect(struct kbdnav_pool *pool);

Eina_Bool           kbdnav_pool_navigate_up(struct kbdnav_pool *pool);
Eina_Bool           kbdnav_pool_navigate_down(struct kbdnav_pool *pool);
Eina_Bool           kbdnav_pool_navigate_left(struct kbdnav_pool *pool);
Eina_Bool           kbdnav_pool_navigate_right(struct kbdnav_pool *pool);

/* sound */
unsigned int sound_init(void);
unsigned int sound_shutdown(void);
Eina_Bool    sound_play(Evas_Object *obj, const char *sound_id);
Eina_Bool    sound_theme_set(const char *xdg_theme);
Eina_Bool    sound_enable_set(Eina_Bool enabled);

#ifdef HAVE_GOOGLE_GADGETS
/* gadget_item */
void        efl_gadget_init(int argc, char **argv);
void        efl_gadget_shutdown(void);
Evas_Object *efl_gadget_new(Evas *, const char *path, const char *uid);
const char  *efl_gadget_path_get(Evas_Object *gadget);
const char  *efl_gadget_uid_get(Evas_Object *gadget);
char        *efl_gadget_uid_make();

/* evas_cairo */
Evas_Object     *evas_image_cairo_new              (Evas *evas, Evas_Coord w, Evas_Coord h, Eina_Bool alpha);
Evas_Object     *evas_image_cairo_new_from_surface (Evas *evas, cairo_surface_t *cairo_surface);
cairo_surface_t *evas_image_cairo_surface_get      (Evas_Object *object);
Eina_Bool        evas_image_cairo_surface_set      (Evas_Object *o, cairo_surface_t *cairo_surface);
void             evas_image_cairo_fill_auto_set    (Evas_Object *o, Eina_Bool enable);
#endif

#endif /* __NETBOOK_LAUNCHER_H__ */
