/* -*- Mode: C; indent-tabs-mode: nil; tab-width: 2 -*-
 *
 * Copyright (C) 2009 Canonical Ltd.
 * Authors:
 *  Gustavo Sverzut Barbieri <gustavo.barbieri@canonical.com>
 *
 * This file is part of Netbook Launcher EFL.
 *
 * Netbook Launcher EFL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * Netbook Launcher EFL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Netbook Launcher EFL.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "netbook-launcher.h"

#include <Elementary.h>
#include <glib/gi18n.h>

struct launchfeedback_ctxt
{
  Evas_Object *inwin;
  Evas_Object *layout;
  Evas_Object *edje;
  Evas_Object *icon1;
  Evas_Object *icon2;
  Ecore_Timer *timeout;
  void (*hide_cb)(void *data);
  void *hide_data;
  unsigned int size;
};

static struct launchfeedback_ctxt ctxt = {
  NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0
};

static const char _EDJE_GROUP_LAUNCHFEEDBACK[] = "e/netbook/launcher/launchfeedback";
static const char _EDJE_PART_ICON1[] = "e.swallow.icon";
static const char _EDJE_PART_ICON2[] = "e.swallow.icon2";
static const char _EDJE_PART_LABEL[] = "e.text.label";
static const char _EDJE_PART_TITLE[] = "e.text.title";
static const char _EDJE_PART_SIZE[] = "e.size";
static const char _EDJE_SIG_SHOW[] = "gui,action,show";
static const char _EDJE_SIG_HIDE[] = "gui,action,hide";
static const char _EDJE_SIG_DISMISS[] = "gui,action,dismiss";
static const char _EDJE_SIG_SRC[] = "e";


static void
_launchfeedback_dismiss(void *data __UNUSED__, Evas_Object *o __UNUSED__, const char *emission __UNUSED__, const char *source __UNUSED__)
{
  launchfeedback_hide();
}

static void
_launchfeedback_del(void *data, Evas *e __UNUSED__, Evas_Object *o __UNUSED__, void *event_info __UNUSED__)
{
  struct launchfeedback_ctxt *ctxt = data;
  ctxt->inwin = NULL;
  ctxt->edje = NULL;
  ctxt->layout = NULL;
  ctxt->icon1 = NULL;
  ctxt->icon2 = NULL;
  if (ctxt->timeout)
  {
    ecore_timer_del(ctxt->timeout);
    ctxt->timeout = NULL;
  }
}

Evas_Object *
launchfeedback_add(Evas_Object *parent)
{
  Evas_Object *inwin, *layout, *edje, *icon1, *icon2;
  Evas *e;
  Evas_Coord w, h, size;

  if (ctxt.inwin)
    return ctxt.inwin;

  inwin = elm_win_inwin_add(parent);
  if (!inwin)
  {
    ERR("could not create launchfeedback system.\n");
    return NULL;
  }
  elm_object_style_set(inwin, "launchfeedback");
  evas_object_event_callback_add
    (inwin,  EVAS_CALLBACK_FREE, _launchfeedback_del, &ctxt);

  layout = elm_layout_add(inwin);
  if (!layout)
  {
    ERR("could not create launchfeedback layout.\n");
    evas_object_del(inwin);
    return NULL;
  }

  edje = elm_layout_edje_get(layout);
  if (!elm_layout_file_set(layout, _nl_prefs.theme, _EDJE_GROUP_LAUNCHFEEDBACK))
  {
    int err = edje_object_load_error_get(edje);
    const char *errmsg = edje_load_error_str(err);
    ERR("cannot load theme '%s', group '%s': %s\n",
        _nl_prefs.theme, _EDJE_GROUP_LAUNCHFEEDBACK, errmsg);
    return 0;
  }

  edje_object_part_geometry_get(edje, _EDJE_PART_ICON1, NULL, NULL, &w, &h);
  if ((w == 0) && (h == 0))
    size = 48;
  else if (w > h)
    size = w;
  else
    size = h;

  size *= elm_object_scale_get(layout);

  e = evas_object_evas_get(edje);

  icon1 = evas_object_image_filled_add(e);
  evas_object_image_load_size_set(icon1, size, size);
  evas_object_size_hint_min_set(icon1, size, size);
  elm_layout_content_set(layout, _EDJE_PART_ICON1, icon1);

  icon2 = evas_object_image_filled_add(e);
  evas_object_image_load_size_set(icon1, size, size);
  elm_layout_content_set(layout, _EDJE_PART_ICON2, icon2);

  edje_object_part_text_set(edje, _EDJE_PART_TITLE, _("Loading..."));

  edje_object_signal_callback_add
    (edje, _EDJE_SIG_DISMISS, _EDJE_SIG_SRC, _launchfeedback_dismiss, NULL);

  elm_win_inwin_content_set(inwin, layout);

  ctxt.inwin = inwin;
  ctxt.layout = layout;
  ctxt.edje = edje;
  ctxt.icon1 = icon1;
  ctxt.icon2 = icon2;
  ctxt.size = size;

  return inwin;
}

static Eina_Bool
_launchfeedback_autohide(void *data __UNUSED__)
{
  launchfeedback_hide();
  return ECORE_CALLBACK_CANCEL;
}

void
launchfeedback_show_full(const char *icon_path, const char *label, double timeout, void (*hide_cb)(void *data), const void *hide_data)
{
  Evas_Coord w, h;

  evas_object_image_file_set(ctxt.icon1, icon_path, NULL);
  evas_object_image_file_set(ctxt.icon2, icon_path, NULL);
  edje_object_part_text_set(ctxt.edje, _EDJE_PART_LABEL, label);
  edje_object_signal_emit(ctxt.edje, _EDJE_SIG_SHOW, _EDJE_SIG_SRC);

  edje_object_part_geometry_get(ctxt.edje, _EDJE_PART_SIZE, NULL, NULL, &w, &h);
  evas_object_size_hint_min_set(ctxt.edje, w, h);
  evas_object_size_hint_min_set(ctxt.layout, w, h);

  elm_win_inwin_activate(ctxt.inwin);
  if (ctxt.timeout)
  {
    if (ctxt.hide_cb)
      ctxt.hide_cb(ctxt.hide_data);
    ecore_timer_del(ctxt.timeout);
  }

  ctxt.hide_cb = hide_cb;
  ctxt.hide_data = (void *)hide_data;

  if (timeout > 0.0)
    ctxt.timeout = ecore_timer_add(timeout, _launchfeedback_autohide, NULL);
  else
    ctxt.timeout = NULL;

  sound_play(ctxt.inwin, "tooltip-popup");
}

void
launchfeedback_show(const char *icon_path, const char *label, double timeout)
{
  launchfeedback_show_full(icon_path, label, timeout, NULL, NULL);
}

void
launchfeedback_hide(void)
{
  if (ctxt.timeout)
  {
    ecore_timer_del(ctxt.timeout);
    ctxt.timeout = NULL;
  }
  if (ctxt.hide_cb)
  {
    ctxt.hide_cb(ctxt.hide_data);
    ctxt.hide_cb = NULL;
  }
  ctxt.hide_data = NULL;

  edje_object_signal_emit(ctxt.edje, _EDJE_SIG_HIDE, _EDJE_SIG_SRC);
  evas_object_hide(ctxt.inwin);
  sound_play(ctxt.inwin, "tooltip-popdown");
}
